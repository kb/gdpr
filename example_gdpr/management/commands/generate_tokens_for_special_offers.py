# -*- encoding: utf-8 -*-
import secrets

from django.core.management.base import BaseCommand

from gdpr.models import Consent, UserConsent, UserConsentUnsubscribe


class Command(BaseCommand):

    help = "Example: Generate tokens for special offers #4403"

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        consent = Consent.objects.get(slug="offers")
        qs = UserConsent.objects.current(consent)
        for user_consent in qs:
            self.stdout.write("- {}".format(user_consent.name))
            UserConsentUnsubscribe.objects.init_user_consent_unsubscribe(
                user_consent, secrets.token_urlsafe()
            )
        self.stdout.write("{} - Complete".format(self.help))
