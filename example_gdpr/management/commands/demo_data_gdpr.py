# -*- encoding: utf-8 -*-
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand
from django.utils.text import slugify

from gdpr.models import (
    Consent,
    ConsentFrequency,
    ConsentMethod,
    Frequency,
    Method,
    get_contact_model,
)


class Command(BaseCommand):

    help = "Demo Data"

    def _init_contact(self):
        contact_model = get_contact_model()
        user_pks = [x.pk for x in get_user_model().objects.all()]
        for pk in user_pks:
            user = get_user_model().objects.get(pk=pk)
            try:
                contact = contact_model.objects.get(user__pk=pk)
                self.stdout.write(
                    "Contact for '{}' already exists".format(user.username)
                )
            except contact_model.DoesNotExist:
                self.stdout.write(
                    "Create contact for: '{}'".format(user.username)
                )
                contact = contact_model(user=user)
                contact.save()

    def _init_consent(
        self, slug, name, message, show_checkbox, methods, frequencies
    ):
        consent = Consent.objects.init_consent(
            slug, name, message, show_checkbox
        )
        for x in frequencies:
            frequency = Frequency.objects.init_frequency(slugify(x), x)
            ConsentFrequency.objects.init_consent_frequency(consent, frequency)
        for x in methods:
            method = Method.objects.init_method(slugify(x), x)
            ConsentMethod.objects.init_consent_method(consent, method)

    def handle(self, *args, **options):
        self.stdout.write(self.help)
        self._init_contact()
        self._init_consent(
            "enquiry",
            "Enquiry Form",
            "Enquiry Form",
            True,
            ["email", "phone"],
            [],
        )
        self._init_consent(
            "product",
            "Product Updates",
            "Product Information",
            True,
            ["Post", "email"],
            ["Daily", "Weekly"],
        )
        self._init_consent(
            "offers",
            "Special Offers",
            "Once a monthly!",
            False,
            [],
            ["Monthly"],
        )
        self.stdout.write("Complete: {}".format(self.help))
