# -*- encoding: utf-8 -*-
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import include, path, re_path

from .views import (
    ContactDetailView,
    DashView,
    HomeView,
    SettingsView,
    UserContactRedirectView,
    UserConsentUnsubscribeListView,
    UserConsentUnsubscribeUpdateView,
)


urlpatterns = [
    re_path(r"^$", view=HomeView.as_view(), name="project.home"),
    re_path(r"^$", view=HomeView.as_view(), name="web.contact"),
    re_path(
        r"^contact/(?P<pk>\d+)/$",
        view=ContactDetailView.as_view(),
        name="contact.detail",
    ),
    re_path(r"^contact/", view=include("contact.urls")),
    re_path(r"^dash/$", view=DashView.as_view(), name="project.dash"),
    re_path(r"^gdpr/", view=include("gdpr.urls")),
    re_path(
        r"^settings/$",
        view=SettingsView.as_view(),
        name="project.settings",
    ),
    re_path(r"^", view=include("login.urls")),
    path(
        "unsubscribe/<str:token>/",
        view=UserConsentUnsubscribeUpdateView.as_view(),
        name="web.unsubscribe",
    ),
    re_path(
        r"^user/consent/unsubscribe/list/$",
        view=UserConsentUnsubscribeListView.as_view(),
        name="example.user.consent.unsubscribe.list",
    ),
    re_path(
        r"^user/(?P<pk>\d+)/redirect/$",
        view=UserContactRedirectView.as_view(),
        name="user.redirect.contact",
    ),
]

urlpatterns += staticfiles_urlpatterns()

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        re_path(r"^__debug__/", include(debug_toolbar.urls))
    ] + urlpatterns
