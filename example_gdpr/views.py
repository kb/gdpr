# -*- encoding: utf-8 -*-
from braces.views import LoginRequiredMixin, StaffuserRequiredMixin
from contact.views import ContactDetailMixin
from django.urls import reverse
from django.views.generic import (
    DetailView,
    ListView,
    RedirectView,
    TemplateView,
    UpdateView,
)

from base.view_utils import BaseMixin, RedirectNextMixin
from gdpr.models import get_contact_model, UserConsent, UserConsentUnsubscribe
from gdpr.views import UserConsentUnsubscribeUpdateMixin


class ContactDetailView(
    LoginRequiredMixin, ContactDetailMixin, BaseMixin, DetailView
):
    template_name = "example/contact_detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            {
                "consent_list": UserConsent.objects.for_user(
                    self.object.user
                ).order_by("consent__name")
            }
        )
        return context


class HomeView(BaseMixin, TemplateView):

    template_name = "example/home.html"


class DashView(LoginRequiredMixin, BaseMixin, TemplateView):

    template_name = "example/dash.html"


class SettingsView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, TemplateView
):

    template_name = "example/settings.html"


class UserContactRedirectView(RedirectView):

    permanent = False

    def get_redirect_url(self, *args, **kwargs):
        pk = kwargs["pk"]
        contact_model = get_contact_model()
        contact = contact_model.objects.get(user__pk=pk)
        return reverse("contact.detail", args=[contact.pk])


class UserConsentUnsubscribeListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):
    model = UserConsentUnsubscribe
    template_name = "example/userconsentunsubscribe_list.html"


class UserConsentUnsubscribeUpdateView(
    RedirectNextMixin, UserConsentUnsubscribeUpdateMixin, BaseMixin, UpdateView
):
    template_name = "example/user_consent_unsubscribe.html"
