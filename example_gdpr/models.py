# -*- encoding: utf-8 -*-
from django.db import models


class ExampleEnquiry(models.Model):

    name = models.CharField(max_length=50)

    class Meta:
        ordering = ("pk",)
        verbose_name = "Example Enquiry"
        verbose_name_plural = "Example Enquiries"

    def __str__(self):
        return "{}".format(self.name)
