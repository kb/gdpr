# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse

from contact.tests.factories import UserContactFactory
from login.tests.fixture import perm_check


@pytest.mark.django_db
def test_contact_detail(perm_check):
    user_contact = UserContactFactory()
    url = reverse("contact.detail", args=[user_contact.contact.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_user_consent_unsubscribe_list(perm_check):
    url = reverse("example.user.consent.unsubscribe.list")
    perm_check.staff(url)
