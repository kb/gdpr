# -*- encoding: utf-8 -*-
import factory

from example_gdpr.models import ExampleEnquiry


class ExampleEnquiryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ExampleEnquiry

    @factory.sequence
    def name(n):
        return "name_{:02d}".format(n)
