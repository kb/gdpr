# -*- encoding: utf-8 -*-
import pytest
import secrets

from datetime import date
from django.urls import reverse
from freezegun import freeze_time
from http import HTTPStatus

from gdpr.models import GdprError, UserConsent, UserConsentUnsubscribe
from gdpr.tests.factories import ConsentFactory
from login.tests.factories import TEST_PASSWORD, UserFactory


@pytest.mark.django_db
def test_unsubscribe_logged_in(client):
    user = UserFactory(first_name="P", last_name="Kimber", username="patrick")
    user_consent = UserConsent.objects.set_consent(
        ConsentFactory(slug="cheese", name="Cheddar"), True, user=user
    )
    assert user_consent.consent_given is True
    token = UserConsentUnsubscribe.objects.create_unsubscribe_token(
        user_consent.user, user_consent.consent.slug
    )
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("web.unsubscribe", args=[token])
    response = client.post(url)
    assert HTTPStatus.FOUND == response.status_code
    assert url == response.url
    user_consent.refresh_from_db()
    assert user_consent.consent_given is False


@pytest.mark.django_db
def test_unsubscribe_logged_in_not(client):
    user = UserFactory(first_name="P", last_name="Kimber", username="patrick")
    user_consent = UserConsent.objects.set_consent(
        ConsentFactory(slug="cheese", name="Cheddar"), True, user=user
    )
    assert user_consent.consent_given is True
    token = UserConsentUnsubscribe.objects.create_unsubscribe_token(
        user_consent.user, user_consent.consent.slug
    )
    url = reverse("web.unsubscribe", args=[token])
    response = client.post(url)
    assert HTTPStatus.FOUND == response.status_code
    assert url == response.url
    user_consent.refresh_from_db()
    assert user_consent.consent_given is False


@pytest.mark.django_db
def test_unsubscribe_token_expired(client):
    user = UserFactory(username="patrick")
    user_consent = UserConsent.objects.set_consent(
        ConsentFactory(slug="cheese", name="Cheddar"), True, user=user
    )
    with freeze_time(date(2019, 1, 1)):
        token = UserConsentUnsubscribe.objects.create_unsubscribe_token(
            user_consent.user, user_consent.consent.slug
        )
    assert 1 == UserConsentUnsubscribe.objects.count()
    user_consent_unsubscribe = UserConsentUnsubscribe.objects.first()
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("web.unsubscribe", args=[token])
    with pytest.raises(GdprError) as e:
        client.post(url)
    assert (
        "Token for unsubscribe request (patrick/cheese/{}) "
        "is not valid (expired on 01/01/2019)".format(
            user_consent_unsubscribe.pk
        )
    ) in str(e.value)


@pytest.mark.django_db
def test_unsubscribe_get(client):
    user_consent = UserConsent.objects.set_consent(
        ConsentFactory(slug="cheese", name="Cheddar"),
        True,
        user=UserFactory(
            first_name="P", last_name="Kimber", username="patrick"
        ),
    )
    token = UserConsentUnsubscribe.objects.create_unsubscribe_token(
        user_consent.user, user_consent.consent.slug
    )
    response = client.get(reverse("web.unsubscribe", args=[token]))
    assert HTTPStatus.OK == response.status_code
    assert "is_valid_user_consent" in response.context
    assert response.context["is_valid_user_consent"] is True
    assert user_consent == response.context["object"]
    assert user_consent == response.context["userconsent"]


@pytest.mark.django_db
def test_unsubscribe_get_expired_token(client):
    user_consent = UserConsent.objects.set_consent(
        ConsentFactory(slug="cheese", name="Cheddar"),
        True,
        user=UserFactory(
            first_name="P", last_name="Kimber", username="patrick"
        ),
    )
    with freeze_time(date(2019, 1, 1)):
        token = UserConsentUnsubscribe.objects.create_unsubscribe_token(
            user_consent.user, user_consent.consent.slug
        )
    response = client.get(reverse("web.unsubscribe", args=[token]))
    assert HTTPStatus.OK == response.status_code
    assert "is_valid_user_consent" in response.context
    assert response.context["is_valid_user_consent"] is False
    assert user_consent == response.context["object"]
    assert user_consent == response.context["userconsent"]


@pytest.mark.django_db
def test_unsubscribe_get_invalid_token(client):
    response = client.get(
        reverse("web.unsubscribe", args=[secrets.token_urlsafe()])
    )
    assert HTTPStatus.OK == response.status_code
    assert "is_valid_user_consent" in response.context
    assert response.context["is_valid_user_consent"] is False
