# -*- encoding: utf-8 -*-
"""
Test the ``UserConsent`` model and manager.

For other tests, see:
gdpr/tests/test_user_consent.py

"""
import pytest
import pytz

from datetime import datetime
from django.utils import timezone

from contact.tests.factories import ContactFactory, ContactEmailFactory
from example_gdpr.tests.factories import ExampleEnquiryFactory
from gdpr.models import UserConsent, UserConsentAudit
from gdpr.tests.factories import ConsentFactory
from login.tests.factories import UserFactory


@pytest.mark.django_db
def test_for_content_object():
    content_object = ExampleEnquiryFactory()
    UserConsent.objects.set_consent(
        ConsentFactory(), True, comment="a", content_object=content_object
    )
    assert ["a"] == [
        x.comment
        for x in UserConsent.objects.for_content_object(content_object)
    ]


@pytest.mark.django_db
def test_for_content_object_multi():
    content_object = ExampleEnquiryFactory()
    UserConsent.objects.set_consent(
        ConsentFactory(), True, comment="a", content_object=content_object
    )
    UserConsent.objects.set_consent(
        ConsentFactory(), True, comment="b", content_object=content_object
    )
    assert set(["a", "b"]) == set(
        [
            x.comment
            for x in UserConsent.objects.for_content_object(content_object)
        ]
    )


@pytest.mark.django_db
def test_for_content_object_different_content_object():
    UserConsent.objects.set_consent(
        ConsentFactory(),
        True,
        comment="a",
        content_object=ExampleEnquiryFactory(),
    )
    assert (
        0
        == UserConsentAudit.objects.for_content_object(
            ExampleEnquiryFactory()
        ).count()
    )


@pytest.mark.django_db
def test_get_email_for_user_contact():
    """The email address for a user might be in the user or contact record.

    .. note:: This test should get the email address for the contact.

    .. note:: The test for getting the email address from a user is in
              ``gdpr/tests/test_user_consent.py``

    """
    consent = ConsentFactory()
    user = UserFactory(email="patrick@kbsoftware.co.uk")
    contact = ContactFactory(user=user)
    ContactEmailFactory(contact=contact, email="web@pkimber.net")
    UserConsent.objects.set_consent(consent, True, user=user)
    assert "web@pkimber.net" == UserConsent.objects.get_email_for_user(user)


@pytest.mark.django_db
def test_set_consent_for_content_object_date_updated():
    content_object = ExampleEnquiryFactory()
    assert 0 == UserConsent.objects.count()
    date_updated = datetime(2015, 11, 29, 20, 13, 5, tzinfo=pytz.utc)
    UserConsent.objects.set_consent(
        ConsentFactory(),
        True,
        content_object=content_object,
        date_updated=date_updated,
    )
    assert 1 == UserConsent.objects.count()
    user_consent = UserConsent.objects.first()
    assert content_object == user_consent.content_object
    assert None == user_consent.user
    assert date_updated == user_consent.date_updated


@pytest.mark.django_db
def test_set_consent_for_content_object_old_record():
    """Handle old user consent records when importing from MailChimp."""
    assert 0 == UserConsent.objects.count()
    assert 0 == UserConsentAudit.objects.count()
    consent = ConsentFactory()
    content_object = ExampleEnquiryFactory()
    UserConsent.objects.set_consent(
        consent, True, content_object=content_object, comment="today"
    )
    # add a consent with an old date
    date_updated = datetime(2015, 11, 29, 20, 13, 5, tzinfo=pytz.utc)
    UserConsent.objects.set_consent(
        consent,
        True,
        content_object=content_object,
        date_updated=date_updated,
        comment="2015",
    )
    assert 1 == UserConsent.objects.count()
    assert 1 == UserConsentAudit.objects.count()
    user_consent = UserConsent.objects.first()
    assert "today" == user_consent.comment
    assert timezone.now().date() == user_consent.date_updated.date()
    # the consent with the old date should be added to the audit
    user_consent_audit = UserConsentAudit.objects.first()
    assert "2015" == user_consent_audit.comment
    assert date_updated == user_consent_audit.date_updated


@pytest.mark.django_db
def test_update_user_for_content_object():
    consent = ConsentFactory()
    enquiry = ExampleEnquiryFactory()
    user_consent = UserConsent.objects.set_consent(
        consent, True, content_object=enquiry
    )
    user_consent.refresh_from_db()
    assert user_consent.user is None
    assert enquiry == user_consent.content_object
    user = UserFactory()
    UserConsent.objects.update_user_for_content_object(enquiry, user)
    assert UserConsent.objects.consent_given(user, consent) is True


@pytest.mark.django_db
def test_update_user_for_content_object_multi():
    consent_1 = ConsentFactory()
    consent_2 = ConsentFactory()
    consent_3 = ConsentFactory()
    enquiry = ExampleEnquiryFactory()
    user_1 = UserFactory()
    UserConsent.objects.set_consent(consent_1, True, content_object=enquiry)
    UserConsent.objects.update_user_for_content_object(enquiry, user_1)
    UserConsent.objects.set_consent(consent_2, True, content_object=enquiry)
    UserConsent.objects.set_consent(consent_3, True, content_object=enquiry)
    user_2 = UserFactory()
    UserConsent.objects.update_user_for_content_object(enquiry, user_2)
    assert UserConsent.objects.consent_given(user_1, consent_1) is True
    assert UserConsent.objects.consent_given(user_2, consent_1) is False
    assert UserConsent.objects.consent_given(user_2, consent_2) is True
    assert UserConsent.objects.consent_given(user_2, consent_3) is True
