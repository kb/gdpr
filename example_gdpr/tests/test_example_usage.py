# -*- encoding: utf-8 -*-
import pytest

from example_gdpr.tests.factories import ExampleEnquiryFactory
from gdpr.models import Consent, UserConsent
from login.tests.factories import UserFactory


def _print_consent(user_consent):
    # print
    print()
    user_consent.refresh_from_db()
    print(
        "'{}' said '{}' on {}".format(
            user_consent.name,
            "Yes" if user_consent.consent_given else "No",
            user_consent.date_updated.strftime("%d/%m/%Y %H:%M:%S"),
        )
    )


@pytest.mark.django_db
def test_example_usage():
    """Example usage.

    We will ignore frequency and method for now.  I don't think they are needed
    for this initial piece of work.

    """
    # create separate consent types for each tick box
    consent = Consent.objects.init_consent(
        "cheese",
        "Cheese Update",
        "Tick the box if you want the latest news on cheese!",
        True,
    )
    # we have a user
    user = UserFactory(username="patrick.kimber")
    # the user ticks the consent checkbox
    consent_given = True
    # initialise the consent record
    user_consent = UserConsent.objects.set_consent(
        consent, consent_given, user=user
    )
    # print
    _print_consent(user_consent)
    # initialise the tick box with the current setting
    consent_given = UserConsent.objects.consent_given(user, consent)
    # the user decides to withdraw consent
    consent_given = False
    user_consent = UserConsent.objects.set_consent(
        consent, consent_given, user=user
    )
    # print
    _print_consent(user_consent)


@pytest.mark.django_db
def test_example_usage_not_logged_in():
    """Example usage.

    We will ignore frequency and method for now.  I don't think they are needed
    for this initial piece of work.

    """
    # create separate consent types for each tick box
    consent = Consent.objects.init_consent(
        "cheese",
        "Cheese Update",
        "Tick the box if you want the latest news on cheese!",
        True,
    )
    enquiry = ExampleEnquiryFactory()
    # the user ticks the consent checkbox
    consent_given = True
    # initialise the consent record
    user_consent = UserConsent.objects.set_consent(
        consent, consent_given, content_object=enquiry
    )
    # print
    _print_consent(user_consent)
    # we now have a user
    user = UserFactory(username="patrick.kimber")
    # update the user on the consent records for the content object
    UserConsent.objects.update_user_for_content_object(enquiry, user)
    # initialise the tick box with the current setting
    consent_given = UserConsent.objects.consent_given(user, consent)
    # print
    _print_consent(user_consent)
