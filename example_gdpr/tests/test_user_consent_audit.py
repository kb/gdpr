# -*- encoding: utf-8 -*-
import pytest

from django.utils import timezone

from gdpr.models import UserConsentAudit
from gdpr.tests.factories import ConsentFactory
from example_gdpr.tests.factories import ExampleEnquiryFactory


@pytest.mark.django_db
def test_for_content_object():
    content_object = ExampleEnquiryFactory()
    UserConsentAudit(
        content_object=content_object,
        consent=ConsentFactory(),
        consent_given=True,
        date_updated=timezone.now(),
        comment="a",
    ).save()
    assert ["a"] == [
        x.comment
        for x in UserConsentAudit.objects.for_content_object(content_object)
    ]


@pytest.mark.django_db
def test_for_content_object_multi():
    content_object = ExampleEnquiryFactory()
    UserConsentAudit(
        content_object=content_object,
        consent=ConsentFactory(),
        consent_given=True,
        date_updated=timezone.now(),
        comment="a",
    ).save()
    UserConsentAudit(
        content_object=content_object,
        consent=ConsentFactory(),
        consent_given=False,
        date_updated=timezone.now(),
        comment="b",
    ).save()
    assert set(["a", "b"]) == set(
        [
            x.comment
            for x in UserConsentAudit.objects.for_content_object(content_object)
        ]
    )


@pytest.mark.django_db
def test_for_content_object_different_content_object():
    UserConsentAudit(
        content_object=ExampleEnquiryFactory(),
        consent=ConsentFactory(),
        consent_given=True,
        date_updated=timezone.now(),
        comment="a",
    ).save()
    assert (
        0
        == UserConsentAudit.objects.for_content_object(
            ExampleEnquiryFactory()
        ).count()
    )
