# -*- encoding: utf-8 -*-
import pytest

from django.core.management import call_command


@pytest.mark.django_db
def test_demo_data():
    call_command("demo_data_gdpr")


@pytest.mark.django_db
def test_generate_tokens_for_special_offers():
    call_command("demo_data_gdpr")
    call_command("generate_tokens_for_special_offers")
