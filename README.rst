GDPR
****

Install
=======

Virtual Environment
-------------------

::

  python3 -m venv venv-gdpr
  source venv-gdpr/bin/activate

  pip install -r requirements/local.txt

Testing
=======

::

  find . -name '*.pyc' -delete
  pytest -x

Release
=======

https://www.kbsoftware.co.uk/docs/
