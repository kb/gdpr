# -*- encoding: utf-8 -*-
from django import forms
from django.db.models.functions import Lower

from .models import (
    Consent,
    ConsentFormSettings,
    ConsentMethod,
    Frequency,
    GdprSettings,
    Method,
    UserConsent,
)


class ConsentFieldFormMixin(object):
    def _setup_checkbox(self, name, consent):
        if consent:
            if not consent.show_checkbox:
                self.fields[name].widget = forms.HiddenInput()
                self.fields[name].initial = True
            self.fields[name].label = consent.message
        else:
            del self.fields[name]

    def _clean_checkbox(self, name, consent):
        data = self.cleaned_data.get(name)
        if consent.consent_required and not data:
            raise forms.ValidationError(
                consent.no_consent_message, code="{}_not_ticked".format(name)
            )
        return data


class ConsentMethodEmptyForm(forms.ModelForm):
    class Meta:
        model = ConsentMethod
        fields = ()


class ConsentForm(forms.ModelForm):

    consent_frequencies = forms.ModelMultipleChoiceField(
        label="Frequency", queryset=Frequency.objects.none(), required=False
    )
    consent_methods = forms.ModelMultipleChoiceField(
        label="Methods", queryset=Method.objects.none(), required=False
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        f = self.fields["description"]
        f.widget.attrs.update({"class": "pure-input-1", "rows": 3})
        data = (("consent_frequencies", Frequency), ("consent_methods", Method))
        for name, model in data:
            f = self.fields[name]
            f.queryset = model.objects.current().order_by(Lower("name"))
            f.widget.attrs.update({"class": "chosen-select pure-input-1"})
        for name in ("name", "message", "no_consent_message"):
            f = self.fields[name]
            f.widget.attrs.update({"class": "pure-input-1", "rows": 3})
        settings = GdprSettings.load()
        if not settings.show_frequency:
            del self.fields["consent_frequencies"]
        if not settings.show_method:
            del self.fields["consent_methods"]

    class Meta:
        model = Consent
        fields = (
            "slug",
            "name",
            "description",
            "show_checkbox",
            "message",
            "consent_required",
            "no_consent_message",
            "consent_frequencies",
            "consent_methods",
        )

    def clean(self):
        cleaned_data = super().clean()
        consent_required = cleaned_data.get("consent_required")
        no_consent_message = cleaned_data.get("no_consent_message")
        if consent_required:
            if not no_consent_message:
                raise forms.ValidationError(
                    (
                        "You have ticked the box to say consent is required. "
                        "Please enter a 'No consent message'."
                    ),
                    code="consent__no_consent_message",
                )


class ConsentFormSettingsForm(forms.ModelForm):
    class Meta:
        model = ConsentFormSettings
        fields = ("slug", "form_notice")


class FrequencyEmptyForm(forms.ModelForm):
    class Meta:
        model = Frequency
        fields = ()


class FrequencyForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        f = self.fields["description"]
        f.widget.attrs.update({"class": "pure-input-1", "rows": 3})
        f = self.fields["name"]
        f.widget.attrs.update({"class": "pure-input-1"})

    class Meta:
        model = Frequency
        fields = ("slug", "name", "description")


class MethodEmptyForm(forms.ModelForm):
    class Meta:
        model = Method
        fields = ()


class MethodForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        f = self.fields["description"]
        f.widget.attrs.update({"class": "pure-input-1", "rows": 3})
        f = self.fields["name"]
        f.widget.attrs.update({"class": "pure-input-1"})

    class Meta:
        model = Method
        fields = ("slug", "name", "description")


class UserConsentEmptyForm(forms.ModelForm):
    class Meta:
        model = UserConsent
        fields = ()


class UserConsentForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        """Exclude existing consent types from the list."""
        user = kwargs.pop("user")
        super().__init__(*args, **kwargs)
        existing = [x.consent.pk for x in UserConsent.objects.filter(user=user)]
        f = self.fields["consent"]
        f.queryset = Consent.objects.current().exclude(pk__in=existing)
        f.widget.attrs.update({"class": "chosen-select"})
        f = self.fields["comment"]
        f.required = True
        f.widget.attrs.update({"class": "pure-input-1", "rows": 3})

    class Meta:
        model = UserConsent
        fields = ("consent", "consent_given", "comment")


class UserConsentGivenForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        f = self.fields["comment"]
        f.initial = ""
        f.required = True
        f.widget.attrs.update({"class": "pure-input-1", "rows": 3})

    class Meta:
        model = UserConsent
        fields = ("consent_given", "comment")
