# -*- encoding: utf-8 -*-
import secrets

from datetime import date
from dateutil.relativedelta import relativedelta
from django.apps import apps
from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.utils import timezone
from reversion import revisions as reversion

from base.model_utils import (
    copy_model_instance_to,
    TimedCreateModifyDeleteModel,
)
from base.singleton import SingletonModel


def get_contact_model():
    return apps.get_model(settings.CONTACT_MODEL)


class GdprError(Exception):
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value

    def __str__(self):
        return repr("%s, %s" % (self.__class__.__name__, self.value))


class FrequencyManager(models.Manager):
    def create_frequency(self, slug, name):
        obj = self.model(slug=slug, name=name)
        obj.save()
        return obj

    def current(self):
        return self.model.objects.exclude(deleted=True)

    def init_frequency(self, slug, name):
        try:
            obj = self.model.objects.get(slug=slug)
            obj.name = name
            obj.save()
        except self.model.DoesNotExist:
            obj = self.create_frequency(slug, name)
        return obj


class Frequency(TimedCreateModifyDeleteModel):
    """Communication frequency e.g. monthly, weekly."""

    slug = models.SlugField(unique=True)
    name = models.CharField(max_length=100, unique=True)
    description = models.TextField(blank=True)
    objects = FrequencyManager()

    class Meta:
        ordering = ["name"]
        verbose_name = "Frequency"
        verbose_name_plural = "Frequencies"

    def __str__(self):
        return "{}".format(self.name)


reversion.register(Frequency)


class GdprSettings(SingletonModel):

    can_download_csv = models.BooleanField(
        default=False,
        help_text="Allow a member of staff to download a CSV of consents",
    )
    show_frequency = models.BooleanField(
        default=False, help_text="Display the frequency option"
    )
    show_method = models.BooleanField(
        default=False, help_text="Display the method option"
    )

    class Meta:
        verbose_name = "GDPR Settings"
        verbose_name_plural = "GDPR Settings"

    def __str__(self):
        return "GDPR Settings: Can download CSV: {}".format(
            self.can_download_csv
        )


reversion.register(GdprSettings)


class MethodManager(models.Manager):
    """Communication methods.

    .. warning:: Do not create an init method or make the name unique.
                 We expect the different consent types to create communication
                 methods with the same name e.g.  ``Post``, ``email``.

    """

    def create_method(self, slug, name):
        obj = self.model(slug=slug, name=name)
        obj.save()
        return obj

    def current(self):
        return self.model.objects.exclude(deleted=True)

    def init_method(self, slug, name):
        try:
            obj = self.model.objects.get(slug=slug)
            obj.name = name
            obj.save()
        except self.model.DoesNotExist:
            obj = self.create_method(slug, name)
        return obj


class Method(TimedCreateModifyDeleteModel):
    """GDPR communication method e.g. post, email, phone."""

    slug = models.SlugField(unique=True)
    name = models.CharField(max_length=100)
    description = models.TextField(blank=True)
    objects = MethodManager()

    class Meta:
        ordering = ["name"]
        verbose_name = "Communication method"
        verbose_name_plural = "Communication methods"

    def __str__(self):
        return "{}".format(self.name)


reversion.register(Method)


class ConsentManager(models.Manager):
    def create_consent(
        self,
        slug,
        name,
        message,
        show_checkbox,
        consent_required=None,
        no_consent_message=None,
    ):
        if consent_required is None:
            consent_required = False
        if no_consent_message is None:
            no_consent_message = ""
        obj = self.model(
            slug=slug,
            name=name,
            message=message,
            show_checkbox=show_checkbox,
            consent_required=consent_required,
            no_consent_message=no_consent_message,
        )
        obj.save()
        return obj

    def current(self):
        return self.model.objects.exclude(deleted=True)

    def get_consent(self, slug):
        try:
            return self.model.objects.get(slug=slug)
        except self.model.DoesNotExist:
            raise GdprError("Consent '{}' does not exist".format(slug))

    def init_consent(
        self,
        slug,
        name,
        message,
        show_checkbox,
        consent_required=None,
        no_consent_message=None,
    ):
        if consent_required is None:
            consent_required = False
        if no_consent_message is None:
            no_consent_message = ""
        try:
            obj = self.model.objects.get(slug=slug)
            obj.name = name
            obj.message = message
            obj.show_checkbox = show_checkbox
            obj.consent_required = consent_required
            obj.no_consent_message = no_consent_message
            obj.save()
        except self.model.DoesNotExist:
            obj = self.create_consent(
                slug,
                name,
                message,
                show_checkbox,
                consent_required,
                no_consent_message,
            )
        return obj


class Consent(TimedCreateModifyDeleteModel):
    """GDPR consent type.

    From:
    GDPR - Development
    https://docs.google.com/spreadsheets/d/1WR1D0zzHO4UPoFxrJ2mx4Gd34PnHJ9K6I-0ZEuPW0u4/edit?usp=sharing

    Your database will be updated so you can track whether a user has given
    consent for the use of their personal data.

    The database will be able to record consent for areas of interest

    1. e.g. marketing campaigns for particular products.
    2. types of communication e.g. contact me via email or post
    3. frequency e.g. once a month or once a year

    Question ref 2 and 3:

    Is the type of communication and frequency related to the user i.e. (one
    record per user) or do these preferences need to be recorded for every
    communication campaign?

    TODO

    - If possible, include a link to the privacy notice with the message.

    """

    slug = models.SlugField(unique=True)
    name = models.CharField(max_length=100, unique=True)
    description = models.TextField(blank=True)
    frequency = models.ManyToManyField(
        Frequency, related_name="consent_frequency", through="ConsentFrequency"
    )
    method = models.ManyToManyField(
        Method, related_name="consent_method", through="ConsentMethod"
    )
    show_checkbox = models.BooleanField(
        default=True,
        help_text=("Display a tick box on the enquiry form to confirm consent"),
    )
    message = models.TextField(
        default=(
            "Your phone number and email address will only be used to "
            "contact you in connection with this enquiry.  Please see our "
            "privacy notice for more information."
        ),
        help_text=(
            "Message explaining to the user why we are collecting their "
            "personal information on the enquiry form"
        ),
    )
    consent_required = models.BooleanField(
        default=False,
        help_text="User must tick the box before submitting the form",
    )
    no_consent_message = models.TextField(
        blank=True,
        default=(
            "Sorry, we will not be able to answer your enquiry "
            "unless you tick the box below to give us permission to use "
            "either your email address or phone number."
        ),
        help_text=(
            "Message explaining to the user why they need to tick the box "
            "(if they didn't)"
        ),
    )
    objects = ConsentManager()

    class Meta:
        ordering = ["name"]
        verbose_name = "Consent"
        verbose_name_plural = "Consent"

    def __str__(self):
        return "{} ('{}')".format(self.name, self.slug)

    def get_frequencies(self):
        return ConsentFrequency.objects.frequencies(self)

    def get_methods(self):
        return ConsentMethod.objects.methods(self)

    def unsubscribe_caption(self):
        """Caption for the submit button on the unsubscribe template."""
        return "Unsubscribe from {}".format(self.name)


reversion.register(Consent)


class ConsentFormSettings(models.Model):
    """Settings for displaying a consent form.

    Just pass the ``form_notice`` to the context and the standard
    ``_form.html`` template will display it.  This can be done by including
    ``ConsentMaintenanceMixin`` and setting ``consent_form_settings`` on your
    View.

    .. tip:: For the future, it might be worth adding a list of ``Consent``
             objects to this model so we can automatically create the consent
             section of the form.

    """

    slug = models.SlugField(unique=True)
    form_notice = models.TextField(
        help_text=(
            "A notice which can be passed to a form. "
            "Will normally be displayed just above the submit button. "
            "The text will be displayed with 'autoescape off', so you can "
            "include URLs."
        )
    )

    class Meta:
        ordering = ["slug"]
        verbose_name = "Consent Form Settings"
        verbose_name_plural = "Consent Form Settings"

    def __str__(self):
        return "{}".format(self.slug)


class ConsentFrequencyManager(models.Manager):
    def create_consent_frequency(self, consent, frequency):
        obj = self.model(consent=consent, frequency=frequency)
        obj.save()
        return obj

    def init_consent_frequency(self, consent, frequency):
        try:
            obj = self.model.objects.get(consent=consent, frequency=frequency)
        except self.model.DoesNotExist:
            obj = self.create_consent_frequency(consent, frequency)
        return obj

    def frequencies(self, consent):
        return self.model.objects.exclude(deleted=True).filter(consent=consent)

    def update_frequencies(self, consent, frequencies, user):
        for x in frequencies:
            self.init_consent_frequency(consent, x)
        # mark the other frequencies as deleted
        qs = (
            self.model.objects.filter(consent=consent)
            .exclude(frequency__in=frequencies)
            .exclude(deleted=True)
        )
        for x in qs:
            x.set_deleted(user)


class ConsentFrequency(TimedCreateModifyDeleteModel):

    consent = models.ForeignKey(Consent, on_delete=models.CASCADE)
    frequency = models.ForeignKey(Frequency, on_delete=models.CASCADE)
    objects = ConsentFrequencyManager()

    class Meta:
        ordering = ["consent__name", "frequency__name"]
        unique_together = ["consent", "frequency"]
        verbose_name = "Consent Frequency"
        verbose_name_plural = "Consent Frequencies"

    def __str__(self):
        return "{} {}".format(self.consent.name, self.frequency.name)


reversion.register(ConsentFrequency)


class ConsentMethodManager(models.Manager):
    def create_consent_method(self, consent, method):
        obj = self.model(consent=consent, method=method)
        obj.save()
        return obj

    def init_consent_method(self, consent, method):
        try:
            obj = self.model.objects.get(consent=consent, method=method)
            if obj.is_deleted:
                obj.undelete()
        except self.model.DoesNotExist:
            obj = self.create_consent_method(consent, method)
        return obj

    def methods(self, consent):
        return self.model.objects.exclude(deleted=True).filter(consent=consent)

    def update_methods(self, consent, methods, user):
        for x in methods:
            self.init_consent_method(consent, x)
        # mark the other methods as deleted
        qs = (
            self.model.objects.filter(consent=consent)
            .exclude(method__in=methods)
            .exclude(deleted=True)
        )
        for x in qs:
            x.set_deleted(user)


class ConsentMethod(TimedCreateModifyDeleteModel):

    consent = models.ForeignKey(Consent, on_delete=models.CASCADE)
    method = models.ForeignKey(Method, on_delete=models.CASCADE)
    objects = ConsentMethodManager()

    class Meta:
        ordering = ["consent__name", "method__name"]
        unique_together = ["consent", "method"]
        verbose_name = "Consent Method"
        verbose_name_plural = "Consent Methods"

    def __str__(self):
        return "{} {}".format(self.consent.name, self.method.name)


reversion.register(ConsentMethod)


class UserConsentMixin(models.Model):
    """GDPR consent for a user.

    .. note:: A user consent will normally have a ``user`` or a
              ``content_object``.

              For an enquiry form, where the user is not logged in, the
              ``content_object`` will be set, but not the ``user``.

              For processes where consent is collected before the user is
              created, we start by linking the consent to the
              ``content_object``, then update the ``user`` field later when the
              user has been created.

    .. note:: Be careful with related_name and related_query_name:
              https://docs.djangoproject.com/en/2.0/topics/db/models/#be-careful-with-related-name-and-related-query-name

    """

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        related_name="+",
        help_text="User who had the opportunity to consent",
    )
    # link to the object in the system which recorded the consent
    content_type = models.ForeignKey(
        ContentType,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        help_text="Object which recorded he consent",
    )
    object_id = models.PositiveIntegerField(blank=True, null=True)
    content_object = GenericForeignKey()
    # type of consent
    consent = models.ForeignKey(Consent, on_delete=models.CASCADE)
    consent_given = models.BooleanField(default=False)
    frequencies = models.ManyToManyField(
        Frequency,
        related_name="%(app_label)s_%(class)s_related",
        help_text="Communication frequencies",
    )
    methods = models.ManyToManyField(
        Method,
        related_name="%(app_label)s_%(class)s_related",
        help_text="Communication methods",
    )
    user_updated = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        related_name="+",
        help_text="User who updated the record",
    )
    comment = models.TextField(
        blank=True, help_text="Reason for editing the consent"
    )
    date_updated = models.DateTimeField(blank=True, null=True)

    class Meta:
        abstract = True
        verbose_name = "User consent"
        verbose_name_plural = "User consents"

    def __str__(self):
        consent_given = "Yes" if self.consent_given else "No"
        return "{} - {} - {}".format(
            self.name, self.consent.name, consent_given
        )

    @property
    def name(self):
        result = ""
        if self.user:
            result = self.user.get_full_name() or self.user.username
        elif self.content_object:
            try:
                result = self.content_object.name
            except AttributeError:
                pass
        return result


class UserConsentManager(models.Manager):
    def _create_object_consent(self, content_object, consent):
        obj = self.model(content_object=content_object, consent=consent)
        obj.save()
        return obj

    def _create_user_consent(self, user, consent):
        obj = self.model(user=user, consent=consent)
        obj.save()
        return obj

    def _object_consent(self, consent, content_object, date_updated):
        try:
            obj = self.for_content_object(content_object).get(consent=consent)
            if obj.date_updated >= date_updated:
                # add to the audit, not the current consent
                obj = UserConsentAudit(
                    content_object=content_object, consent=consent
                )
                obj.save()
            else:
                # add a copy of the current record to the audit
                UserConsentAudit.objects.audit_user_consent(obj)
        except self.model.DoesNotExist:
            obj = self._create_object_consent(content_object, consent)
        return obj

    def _user_consent(self, consent, user, date_updated):
        try:
            obj = self.model.objects.get(user=user, consent=consent)
            if obj.date_updated >= date_updated:
                # add to the audit, not the current consent
                obj = UserConsentAudit(user=user, consent=consent)
                obj.save()
            else:
                # add a copy of the current record to the audit
                UserConsentAudit.objects.audit_user_consent(obj)
        except self.model.DoesNotExist:
            obj = self._create_user_consent(user, consent)
        return obj

    def consent_exists_for_user(
        self, consent, user, consent_given, date_updated, comment
    ):
        """When importing consents from MailChimp, don't duplicate.

        Check the consent record and the consent audit.

        .. note:: My original code included logic to check consents for a
                  ``content_object``.
                  The code is not needed now, so I removed it:
                  https://gitlab.com/kb/gdpr/commit/7352814c5c1c08f701d94b544533722ad9f5dc72

        """
        result = False
        try:
            self.model.objects.get(
                user=user,
                consent=consent,
                consent_given=consent_given,
                date_updated=date_updated,
                comment=comment,
            )
            result = True
        except self.model.DoesNotExist:
            qs = UserConsentAudit.objects.filter(
                user=user,
                consent=consent,
                consent_given=consent_given,
                date_updated=date_updated,
                comment=comment,
            )
            result = bool(qs.count())
        return result

    def consent_given(self, user, consent):
        result = False
        try:
            user_consent = self.model.objects.get(user=user, consent=consent)
            result = user_consent.consent_given
        except self.model.DoesNotExist:
            pass
        return result

    def consent_given_for_all(self, user):
        """Did the user give their consent on all records?"""
        qs = self.model.objects.filter(user=user)
        consent_given = [x.consent_given for x in qs]
        return bool(consent_given) and all(consent_given)

    def current(self, consent):
        return self.model.objects.filter(consent=consent)

    def for_content_object(self, obj):
        return self.model.objects.filter(
            content_type=ContentType.objects.get_for_model(obj),
            object_id=obj.pk,
        )

    def for_user(self, user):
        return self.model.objects.filter(user=user)

    def get_email_for_user(self, user):
        """Get the current email address for a user.

        .. note:: All of our projects add a contact model to the user.

        .. warning:: You will need to check your contact model has an ``email``
                     method which returns the current email address for the
                     contact.

        """
        result = ""
        contact_model = get_contact_model()
        try:
            contact = contact_model.objects.get(user=user)
            result = contact.email()
        except contact_model.DoesNotExist:
            result = user.email
        return result

    def set_consent(
        self,
        consent,
        consent_given,
        user=None,
        content_object=None,
        user_updated=None,
        comment=None,
        date_updated=None,
        frequencies=None,
        methods=None,
    ):
        """Update the consent for the user (or 'content_object').

        Keyword arguments:
        user -- consent must have a ``user`` (or ``content_object``)
        content_object -- consent must have a ``content_object`` (or ``user``)
        user_updated -- the user who updated the record (default ``user``)
        comment -- why was the consent was updated (default "")
        date_updated -- when was the consent updated (default now) (see note)
        frequencies -- (not currently used or tested)
        methods -- (not currently used or tested)

        .. note:: If we don't have a user (e.g. on an enquiry form), we still
                  need to record consent.  This is why we have the option of
                  using a ``content_object``.

        .. note:: If the ``date_updated`` is older than the current consent
                  record, then we add the new data to the audit.

        """
        # default values
        if comment is None:
            comment = ""
        if date_updated is None:
            date_updated = timezone.now()
        if frequencies is None:
            frequencies = []
        if methods is None:
            methods = []
        # update an existing record (if there is one)
        if user:
            obj = self._user_consent(consent, user, date_updated)
            if user_updated is None:
                user_updated = user
        elif content_object:
            obj = self._object_consent(consent, content_object, date_updated)
        else:
            raise GdprError(
                "To record consent, we need a user or 'content_object'"
            )
        # add the rest of the data
        obj.consent_given = consent_given
        obj.date_updated = date_updated
        obj.user_updated = user_updated
        obj.comment = comment
        obj.save()
        obj.frequencies.clear()
        for x in frequencies:
            obj.frequencies.add(x)
        obj.methods.clear()
        for x in methods:
            obj.methods.add(x)
        return obj

    def update_user_for_content_object(self, content_object, user):
        qs = self.for_content_object(content_object).exclude(user__isnull=False)
        pks = [x.pk for x in qs]
        for pk in pks:
            obj = self.model.objects.get(pk=pk)
            obj.user = user
            obj.save()

    def user_consent_given(self, consent):
        """Which users have given consent?

        Returns a list of primary keys for the users who gave consent.

        """
        return list(
            self.model.objects.filter(
                consent=consent, consent_given=True
            ).values_list("user__pk", flat=True)
        )


class UserConsent(UserConsentMixin):

    objects = UserConsentManager()

    class Meta:
        verbose_name = "User consent"
        verbose_name_plural = "User consents"


reversion.register(UserConsent)


class UserConsentAuditManager(models.Manager):
    def audit_user_consent(self, user_consent):
        audit = copy_model_instance_to(user_consent, self.model)
        audit.save()
        for x in user_consent.frequencies.all():
            audit.frequencies.add(x)
        for x in user_consent.methods.all():
            audit.methods.add(x)
        return audit

    def current(self, user_consent):
        return self.model.objects.filter(
            consent=user_consent.consent, user=user_consent.user
        )

    def for_content_object(self, obj):
        return self.model.objects.filter(
            content_type=ContentType.objects.get_for_model(obj),
            object_id=obj.pk,
        )


class UserConsentAudit(UserConsentMixin):

    objects = UserConsentAuditManager()

    class Meta:
        verbose_name = "User consent audit"
        verbose_name_plural = "User consent audit"


class UserConsentUnsubscribeManager(models.Manager):
    def _create_user_consent_unsubscribe(self, user_consent, token):
        if not user_consent.consent_given:
            raise GdprError(
                "'{}' has not given consent for '{}' (id {}) so we "
                "should not be creating an unsubscribe token.".format(
                    user_consent.name,
                    user_consent.consent.slug,
                    user_consent.pk,
                )
            )
        x = self.model(user_consent=user_consent, token=token)
        x.save()
        return x

    def create_unsubscribe_token(self, user, consent_slug):
        """Create an unsubscribe token for a user consent.

        e.g::

          url = UserConsentUnsubscribe.objects.create_unsubscribe_url(
              user, "gdpr-special-offers",
          )

        """
        try:
            user_consent = UserConsent.objects.get(
                user=user, consent__slug=consent_slug
            )
        except UserConsent.DoesNotExist:
            raise GdprError(
                "Cannot find a '{}' consent for '{}'".format(
                    consent_slug, user.username
                )
            )
        token = secrets.token_urlsafe()
        self._create_user_consent_unsubscribe(user_consent, token)
        return token


class UserConsentUnsubscribe(TimedCreateModifyDeleteModel):
    token = models.URLField(unique=True)
    user_consent = models.ForeignKey(UserConsent, on_delete=models.CASCADE)
    objects = UserConsentUnsubscribeManager()

    class Meta:
        verbose_name = "Unsubscribe user consent"
        verbose_name_plural = "Unsubscribe user consent"

    def __str__(self):
        return "{} - {} (Token {})".format(
            self.user_consent.name,
            self.user_consent.consent.name,
            "Deleted" if self.is_deleted else "Active",
        )

    def is_valid_token(self):
        """Was the token created more than 3 months ago?"""
        three_months_ago = date.today() + relativedelta(months=-3)
        return self.created.date() > three_months_ago

    def unsubscribe_comment(self):
        """Pretty message for the user consent object on unsubscribe."""
        return "Unsubscribed on {}".format(self.created.strftime("%d/%m/%Y"))


reversion.register(UserConsentUnsubscribe)
