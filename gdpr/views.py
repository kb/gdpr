# -*- encoding: utf-8 -*-
import csv
import logging

from braces.views import LoginRequiredMixin, StaffuserRequiredMixin
from django.contrib import messages
from django.contrib.auth import get_user_model, REDIRECT_FIELD_NAME
from django.contrib.auth.decorators import user_passes_test
from django.db import transaction
from django.db.models.functions import Lower
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.utils import timezone
from django.views.generic import CreateView, DetailView, ListView, UpdateView

from base.view_utils import BaseMixin, RedirectNextMixin
from .forms import (
    ConsentForm,
    ConsentFormSettingsForm,
    FrequencyEmptyForm,
    FrequencyForm,
    MethodEmptyForm,
    MethodForm,
    UserConsentEmptyForm,
    UserConsentForm,
    UserConsentGivenForm,
)
from .models import (
    Consent,
    ConsentFormSettings,
    ConsentFrequency,
    ConsentMethod,
    Frequency,
    GdprError,
    GdprSettings,
    Method,
    UserConsent,
    UserConsentAudit,
    UserConsentUnsubscribe,
)


logger = logging.getLogger(__name__)


def _get_response(file_name):
    """Create the HttpResponse object with the appropriate CSV header."""
    response = HttpResponse(content_type="text/csv")
    response["Content-Disposition"] = 'attachment; filename="{}"'.format(
        file_name
    )
    return response


def _report_consent_user(consent, response):
    csv_writer = csv.writer(response, dialect="excel")
    csv_writer.writerow(("name", "email", "date", "consent"))
    qs = UserConsent.objects.current(consent).order_by("-date_updated", "pk")
    for x in qs:
        consent_given = date_updated = email = ""
        if x.user:
            email = UserConsent.objects.get_email_for_user(x.user)
        if x.consent_given:
            consent_given = "Y"
        if x.date_updated:
            date_updated = x.date_updated.strftime("%d/%m/%Y")
        l = [x.name, email, date_updated, consent_given]
        csv_writer.writerow(l)
    return response


@user_passes_test(lambda u: u.is_staff)
def report_consent_user_download(request, pk):
    today = timezone.now()
    consent = Consent.objects.get(pk=pk)
    file_name = "gdpr-consent-{}-{}-{:02d}-{:02d}.csv".format(
        consent.slug, today.year, today.month, today.day
    )
    response = _get_response(file_name)
    return _report_consent_user(consent, response)


class ConsentDetailView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, DetailView
):

    model = Consent

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({"gdpr_settings": GdprSettings.load()})
        return context


class ConsentListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):

    paginate_by = 20

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({"gdpr_settings": GdprSettings.load()})
        return context

    def get_queryset(self):
        return Consent.objects.all().order_by(Lower("name"))


class ConsentFormMixin:

    form_class = ConsentForm
    model = Consent

    def form_valid(self, form):
        with transaction.atomic():
            self.object = form.save()
            settings = GdprSettings.load()
            # update many to many using 'through' models
            if settings.show_frequency:
                consent_frequencies = form.cleaned_data["consent_frequencies"]
                ConsentFrequency.objects.update_frequencies(
                    self.object, consent_frequencies, self.request.user
                )
            if settings.show_method:
                consent_methods = form.cleaned_data["consent_methods"]
                ConsentMethod.objects.update_methods(
                    self.object, consent_methods, self.request.user
                )
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse("gdpr.consent.detail", args=[self.object.pk])


class ConsentMaintenanceMixin(object):
    def _save_consent(self, consent, consent_given, user, content_object=None):
        return UserConsent.objects.set_consent(
            consent, consent_given, user=user, content_object=content_object
        )

    def _consent(self, slug):
        return Consent.objects.get_consent(slug)

    def _user_consent_given(self, user, consent):
        return UserConsent.objects.consent_given(user, consent)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if hasattr(self, "consent_form_settings"):
            consent_form_settings = ConsentFormSettings.objects.get(
                slug=self.consent_form_settings
            )
            show = True
            if hasattr(self, "show_form_notice"):
                show = self.show_form_notice
            if show:
                context.update(
                    {"form_notice": consent_form_settings.form_notice}
                )
        return context


class ConsentDisplayMixin(object):
    def _consent_given(self, slug):
        consent = Consent.objects.get(slug=slug)
        return UserConsent.objects.consent_given(self.request.user, consent)


class ConsentCreateView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    ConsentFormMixin,
    BaseMixin,
    CreateView,
):
    pass


class ConsentUpdateView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    ConsentFormMixin,
    BaseMixin,
    UpdateView,
):
    def get_initial(self):
        result = super().get_initial()
        frequency_pks = [x.frequency.pk for x in self.object.get_frequencies()]
        method_pks = [x.method.pk for x in self.object.get_methods()]
        result.update(
            dict(consent_frequencies=frequency_pks, consent_methods=method_pks)
        )
        return result


class ConsentUserListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):

    paginate_by = 20

    def _consent(self):
        pk = self.kwargs["pk"]
        consent = Consent.objects.get(pk=pk)
        return consent

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        consent = self._consent()
        context.update(dict(consent=consent, gdpr_settings=GdprSettings.load()))
        return context

    def get_queryset(self):
        consent = self._consent()
        return UserConsent.objects.current(consent).order_by("-date_updated")


class FormSettingsCreateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, CreateView
):

    form_class = ConsentFormSettingsForm
    model = ConsentFormSettings

    def get_success_url(self):
        return reverse("gdpr.form.settings.list")


class FormSettingsListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):

    paginate_by = 20

    def get_queryset(self):
        return ConsentFormSettings.objects.all().order_by(Lower("slug"))


class FormSettingsUpdateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):

    form_class = ConsentFormSettingsForm
    model = ConsentFormSettings

    def get_success_url(self):
        return reverse("gdpr.form.settings.list")


class FrequencyCreateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, CreateView
):
    model = Frequency
    form_class = FrequencyForm

    def get_success_url(self):
        return reverse("gdpr.frequency.list")


class FrequencyDeleteView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    form_class = FrequencyEmptyForm
    model = Frequency

    template_name = "gdpr/frequency_delete_form.html"

    def form_valid(self, form):
        with transaction.atomic():
            self.object = form.save(commit=False)
            self.object.set_deleted(self.request.user)
            messages.info(
                self.request,
                "Deleted '{}' communication frequency.".format(
                    self.object.name
                ),
            )
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse("gdpr.frequency.list")


class FrequencyListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):

    paginate_by = 20

    def get_queryset(self):
        return Frequency.objects.current().order_by(Lower("name"))


class FrequencyUpdateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):

    form_class = FrequencyForm
    model = Frequency

    def get_success_url(self):
        return reverse("gdpr.frequency.list")


class MethodCreateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, CreateView
):
    model = Method
    form_class = MethodForm

    def get_success_url(self):
        return reverse("gdpr.method.list")


class MethodDeleteView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    form_class = MethodEmptyForm
    model = Method

    template_name = "gdpr/method_delete_form.html"

    def form_valid(self, form):
        with transaction.atomic():
            self.object = form.save(commit=False)
            self.object.set_deleted(self.request.user)
            messages.info(
                self.request,
                "Deleted '{}' communication method.".format(self.object.name),
            )
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse("gdpr.method.list")


class MethodListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):

    paginate_by = 20

    def get_queryset(self):
        return Method.objects.current().order_by(Lower("name"))


class MethodUpdateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):

    form_class = MethodForm
    model = Method

    def get_success_url(self):
        return reverse("gdpr.method.list")


class UserConsentUnsubscribeUpdateMixin:
    form_class = UserConsentEmptyForm

    def _check_expiry(self):
        user_consent_unsubscribe = self._user_consent_unsubscribe_raise()
        if user_consent_unsubscribe.is_valid_token():
            pass
        else:
            raise GdprError(
                "Token for unsubscribe request ({}/{}/{}) is not valid "
                "(expired on {})".format(
                    user_consent_unsubscribe.user_consent.user.username,
                    user_consent_unsubscribe.user_consent.consent.slug,
                    user_consent_unsubscribe.pk,
                    user_consent_unsubscribe.created.strftime("%d/%m/%Y"),
                )
            )

    def _unsubscribe_comment(self):
        user_consent_unsubscribe = self._user_consent_unsubscribe_raise()
        return user_consent_unsubscribe.unsubscribe_comment()

    def _token(self):
        return self.kwargs.get("token")

    def _user_consent(self):
        result = None
        user_consent_unsubscribe = self._user_consent_unsubscribe()
        if user_consent_unsubscribe:
            result = user_consent_unsubscribe.user_consent
        return result

    def _user_consent_unsubscribe(self):
        result = None
        token = self._token()
        if token:
            try:
                result = UserConsentUnsubscribe.objects.get(token=token)
            except UserConsentUnsubscribe.DoesNotExist:
                pass
        return result

    def _user_consent_unsubscribe_raise(self):
        """Find the user consent unsubscribe object (or raise an exception)."""
        user_consent_unsubscribe = self._user_consent_unsubscribe()
        if not user_consent_unsubscribe:
            raise GdprError(
                "Cannot find unsubscribe request for user "
                "consent (token '{}')".format(self._token())
            )
        return user_consent_unsubscribe

    def form_valid(self, form):
        self._check_expiry()
        with transaction.atomic():
            self.object = form.save(commit=False)
            UserConsent.objects.set_consent(
                self.object.consent,
                False,
                user=self.object.user,
                content_object=self._user_consent_unsubscribe(),
                user_updated=self.object.user,
                comment=self._unsubscribe_comment(),
            )
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        is_valid_user_consent = False
        user_consent = self._user_consent()
        user_consent_unsubscribe = self._user_consent_unsubscribe()
        if user_consent and user_consent_unsubscribe:
            if user_consent_unsubscribe.is_valid_token():
                is_valid_user_consent = True
        context.update(dict(is_valid_user_consent=is_valid_user_consent))
        return context

    def get_object(self):
        result = self._user_consent()
        if not result:
            result = UserConsent()
            logger.info(
                "Cannot find unsubscribe request for "
                "token: {}".format(self._token())
            )
        return result

    def get_success_url(self):
        return self.request.path


class UserConsentAuditListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):

    paginate_by = 20

    def _user_consent(self):
        pk = self.kwargs["pk"]
        user_consent = UserConsent.objects.get(pk=pk)
        return user_consent

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user_consent = self._user_consent()
        context.update(dict(user_consent=user_consent))
        return context

    def get_queryset(self):
        user_consent = self._user_consent()
        return UserConsentAudit.objects.current(user_consent).order_by(
            "-date_updated"
        )


class UserConsentMixin:

    model = UserConsent

    def form_valid(self, form):
        with transaction.atomic():
            self.object = form.save(commit=False)
            UserConsent.objects.set_consent(
                self.object.consent,
                self.object.consent_given,
                user=self._consent_user(),
                user_updated=self.request.user,
                comment=self.object.comment,
            )
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        consent_user = self._consent_user()
        context.update(dict(consent_user=consent_user))
        return context

    def get_success_url(self):
        next_url = self.request.POST.get(REDIRECT_FIELD_NAME)
        if next_url:
            return next_url
        else:
            consent_user = self._consent_user()
            return reverse("user.redirect.contact", args=[consent_user.pk])


class UserConsentCreateView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    RedirectNextMixin,
    UserConsentMixin,
    BaseMixin,
    CreateView,
):

    form_class = UserConsentForm

    def _consent_user(self):
        pk = self.kwargs["pk"]
        consent_user = get_user_model().objects.get(pk=pk)
        return consent_user

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update(dict(user=self._consent_user()))
        return kwargs


class UserConsentUpdateView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    RedirectNextMixin,
    UserConsentMixin,
    BaseMixin,
    UpdateView,
):

    form_class = UserConsentGivenForm

    def _consent_user(self):
        return self.object.user

    def get_initial(self):
        result = super().get_initial()
        result.update(dict(comment=""))
        return result
