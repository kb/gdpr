# -*- encoding: utf-8 -*-
import pytest

from django.db.utils import IntegrityError

from gdpr.models import Method
from login.tests.factories import UserFactory
from .factories import MethodFactory


@pytest.mark.django_db
def test_create_method():
    method = Method.objects.create_method("postal", "Postal")
    assert "Postal" == method.name
    assert "postal" == method.slug


@pytest.mark.django_db
def test_create_method_duplicate():
    Method.objects.create_method("postal", "Postal")
    with pytest.raises(IntegrityError) as e:
        Method.objects.create_method("postal", "Postal")
    assert "duplicate key value violates unique" in str(e.value)


@pytest.mark.django_db
def test_current():
    MethodFactory(slug="1")
    MethodFactory(slug="2").set_deleted(UserFactory())
    MethodFactory(slug="3")
    assert ["1", "3"] == [x.slug for x in Method.objects.current()]


@pytest.mark.django_db
def test_str():
    method = MethodFactory(name="email")
    assert "email" == str(method)
