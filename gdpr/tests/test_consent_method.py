# -*- encoding: utf-8 -*-
import pytest

from gdpr.models import Consent, ConsentMethod
from .factories import MethodFactory


@pytest.mark.django_db
def test_create_consent_method():
    consent = Consent.objects.init_consent("events", "Events", "Listing", True)
    consent_method = ConsentMethod.objects.create_consent_method(
        consent, MethodFactory(name="Post")
    )
    assert consent == consent_method.consent
    assert "Post" == consent_method.method.name


@pytest.mark.django_db
def test_init_consent_method():
    consent = Consent.objects.init_consent("events", "Events", "Listing", True)
    method = MethodFactory(name="Post")
    consent_method = ConsentMethod.objects.init_consent_method(consent, method)
    ConsentMethod.objects.init_consent_method(consent, method)
    assert "Post" == consent_method.method.name
    assert 1 == consent.method.count()


@pytest.mark.django_db
def test_ordering():
    consent = Consent.objects.init_consent("events", "Events", "Listing", True)
    ConsentMethod.objects.init_consent_method(
        consent, MethodFactory(name="email")
    )
    ConsentMethod.objects.init_consent_method(
        consent, MethodFactory(name="Post")
    )
    assert ["email", "Post"] == [
        x.method.name for x in ConsentMethod.objects.all()
    ]


@pytest.mark.django_db
def test_str():
    consent = Consent.objects.init_consent("events", "Events", "Listing", True)
    consent_method = ConsentMethod.objects.init_consent_method(
        consent, MethodFactory(name="Post")
    )
    assert "Events Post" == str(consent_method)
