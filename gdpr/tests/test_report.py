# -*- encoding: utf-8 -*-
import io
import pytest

from freezegun import freeze_time

from gdpr.models import UserConsent
from gdpr.tests.factories import ConsentFactory
from gdpr.views import _report_consent_user
from login.tests.factories import UserFactory


@pytest.mark.django_db
def test_report_consent_user():
    consent = ConsentFactory()
    with freeze_time("2017-05-21"):
        user_1 = UserFactory(
            first_name="A", last_name="Kimber", email="a@test.com"
        )
        UserConsent.objects.set_consent(consent, True, user=user_1)
        user_2 = UserFactory(
            first_name="B", last_name="Orange", email="b@b.com"
        )
        UserConsent.objects.set_consent(consent, False, user=user_2)
    with io.StringIO() as response:
        result = _report_consent_user(consent, response)
        response.seek(0)
        result = response.readlines()
    header = "name,email,date,consent\r\n"
    assert [
        header,
        "A Kimber,a@test.com,21/05/2017,Y\r\n",
        "B Orange,b@b.com,21/05/2017,\r\n",
    ] == result
