# -*- encoding: utf-8 -*-
import pytest

from gdpr.models import Consent, ConsentFrequency, ConsentMethod, GdprError
from .factories import ConsentFactory, FrequencyFactory, MethodFactory


@pytest.mark.django_db
def test_create_consent():
    consent = Consent.objects.create_consent(
        "olive",
        "Olive Update",
        "For people who love Olives!",
        True,
        True,
        "Please tick the box.",
    )
    assert "olive" == consent.slug
    assert "Olive Update" == consent.name
    assert "For people who love Olives!" == consent.message
    assert consent.show_checkbox is True
    assert consent.consent_required is True
    assert "Please tick the box." == consent.no_consent_message


@pytest.mark.django_db
def test_get_consent():
    ConsentFactory(slug="apple")
    Consent.objects.get_consent("apple")


@pytest.mark.django_db
def test_get_consent_does_not_exist():
    with pytest.raises(GdprError) as e:
        Consent.objects.get_consent("apple")
    assert "Consent 'apple' does not exist" in str(e.value)


@pytest.mark.django_db
def test_get_methods():
    consent = Consent.objects.create_consent(
        "olive", "Olive Update", "For people who love Olives!", False
    )
    ConsentMethod.objects.init_consent_method(consent, MethodFactory(name="a"))
    ConsentMethod.objects.init_consent_method(consent, MethodFactory(name="b"))
    assert set(["a", "b"]) == {x.method.name for x in consent.get_methods()}
    assert "olive" == consent.slug
    assert "Olive Update" == consent.name
    assert "For people who love Olives!" == consent.message
    assert consent.show_checkbox is False
    assert consent.consent_required is False
    assert "" == consent.no_consent_message


@pytest.mark.django_db
def test_init_consent():
    consent = Consent.objects.init_consent(
        "cheese",
        "Cheese Update",
        "Help with your cheese board",
        True,
        True,
        "Please tick the box.",
    )
    assert "cheese" == consent.slug
    assert "Cheese Update" == consent.name
    assert "Help with your cheese board" == consent.message
    assert consent.show_checkbox is True
    assert consent.consent_required is True
    assert "Please tick the box." == consent.no_consent_message


@pytest.mark.django_db
def test_frequency():
    consent = Consent.objects.init_consent(
        "events", "Events", "List of Events", True
    )
    ConsentFrequency.objects.create_consent_frequency(
        consent, FrequencyFactory(name="Monthly")
    )
    ConsentFrequency.objects.create_consent_frequency(
        consent, FrequencyFactory(name="Daily")
    )
    assert ["Daily", "Monthly"] == [x.name for x in consent.frequency.all()]


@pytest.mark.django_db
def test_method():
    consent = Consent.objects.init_consent("events", "Events", "Listings", True)
    ConsentMethod.objects.create_consent_method(
        consent, MethodFactory(name="Post")
    )
    ConsentMethod.objects.create_consent_method(
        consent, MethodFactory(name="email")
    )
    assert ["email", "Post"] == [x.name for x in consent.method.all()]


@pytest.mark.django_db
def test_str():
    consent = ConsentFactory(slug="enquiry", name="Enquiry Form")
    assert "Enquiry Form ('enquiry')" == str(consent)


@pytest.mark.django_db
def test_unsubscribe_caption():
    """Caption for the submit button on the unsubscribe template."""
    consent = ConsentFactory(name="Special Offers")
    assert "Unsubscribe from Special Offers" == consent.unsubscribe_caption()
