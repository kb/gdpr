# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse

from login.tests.fixture import perm_check
from gdpr.models import Consent, ConsentMethod, Method, UserConsent
from gdpr.tests.factories import (
    ConsentFactory,
    ConsentFormSettingsFactory,
    FrequencyFactory,
    MethodFactory,
)
from login.tests.factories import UserFactory


@pytest.mark.django_db
def test_consent_list(perm_check):
    url = reverse("gdpr.consent.list")
    perm_check.staff(url)


@pytest.mark.django_db
def test_consent_create(perm_check):
    Consent.objects.init_consent("enquiry", "Enquiry Form", "Enquiries", True)
    url = reverse("gdpr.consent.create")
    perm_check.staff(url)


@pytest.mark.django_db
def test_consent_update(perm_check):
    consent = ConsentFactory()
    url = reverse("gdpr.consent.update", args=[consent.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_consent_user_list(perm_check):
    consent = ConsentFactory()
    UserConsent.objects.set_consent(consent, True, user=UserFactory())
    url = reverse("gdpr.consent.user.list", args=[consent.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_form_settings_create(perm_check):
    url = reverse("gdpr.form.settings.create")
    perm_check.staff(url)


@pytest.mark.django_db
def test_form_settings_list(perm_check):
    url = reverse("gdpr.form.settings.list")
    perm_check.staff(url)


@pytest.mark.django_db
def test_form_settings_update(perm_check):
    settings = ConsentFormSettingsFactory()
    url = reverse("gdpr.form.settings.update", args=[settings.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_frequency_create(perm_check):
    url = reverse("gdpr.frequency.create")
    perm_check.staff(url)


@pytest.mark.django_db
def test_frequency_delete(perm_check):
    frequency = FrequencyFactory()
    url = reverse("gdpr.frequency.delete", args=[frequency.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_frequency_list(perm_check):
    FrequencyFactory()
    FrequencyFactory()
    url = reverse("gdpr.frequency.list")
    perm_check.staff(url)


@pytest.mark.django_db
def test_frequency_update(perm_check):
    frequency = FrequencyFactory()
    url = reverse("gdpr.frequency.update", args=[frequency.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_method_create(perm_check):
    url = reverse("gdpr.method.create")
    perm_check.staff(url)


@pytest.mark.django_db
def test_method_delete(perm_check):
    method = MethodFactory()
    url = reverse("gdpr.method.delete", args=[method.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_method_list(perm_check):
    MethodFactory()
    MethodFactory()
    url = reverse("gdpr.method.list")
    perm_check.staff(url)


@pytest.mark.django_db
def test_method_update(perm_check):
    method = MethodFactory()
    url = reverse("gdpr.method.update", args=[method.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_user_consent_audit_list(perm_check):
    user = UserFactory()
    consent = ConsentFactory()
    UserConsent.objects.set_consent(consent, True, user=user)
    UserConsent.objects.set_consent(consent, False, user=user)
    user_consent = UserConsent.objects.set_consent(consent, True, user=user)
    url = reverse("gdpr.user.consent.audit.list", args=[user_consent.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_user_consent_create(perm_check):
    user = UserFactory()
    url = reverse("gdpr.user.consent.create", args=[user.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_user_consent_download(perm_check):
    consent = ConsentFactory()
    url = reverse("gdpr.consent.user.download", args=[consent.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_user_consent_update(perm_check):
    consent = ConsentFactory()
    user_consent = UserConsent.objects.set_consent(
        consent, True, user=UserFactory()
    )
    url = reverse("gdpr.user.consent.update", args=[user_consent.pk])
    perm_check.staff(url)
