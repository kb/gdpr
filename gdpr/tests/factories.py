# -*- encoding: utf-8 -*-
import factory

from gdpr.models import (
    Consent,
    ConsentFormSettings,
    Frequency,
    GdprSettings,
    Method,
)


class ConsentFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Consent

    @factory.sequence
    def name(n):
        return "name_{:02d}".format(n)

    @factory.sequence
    def slug(n):
        return "slug_{:02d}".format(n)


class ConsentFormSettingsFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ConsentFormSettings

    @factory.sequence
    def form_notice(n):
        return "name_{:02d}".format(n)

    @factory.sequence
    def slug(n):
        return "slug_{:02d}".format(n)


class FrequencyFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Frequency

    @factory.sequence
    def name(n):
        return "name_{:02d}".format(n)

    @factory.sequence
    def slug(n):
        return "slug_{:02d}".format(n)


class GdprSettingsFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = GdprSettings


class MethodFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Method

    @factory.sequence
    def name(n):
        return "name_{:02d}".format(n)

    @factory.sequence
    def slug(n):
        return "slug_{:02d}".format(n)
