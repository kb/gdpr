# -*- encoding: utf-8 -*-
import pytest
import secrets

from datetime import date
from django.db import IntegrityError
from freezegun import freeze_time

from gdpr.models import GdprError, UserConsent, UserConsentUnsubscribe
from gdpr.tests.factories import ConsentFactory
from login.tests.factories import UserFactory


@pytest.mark.django_db
def test_create_unsubscribe_url():
    user = UserFactory(first_name="P", last_name="Kimber", username="patrick")
    UserConsent.objects.set_consent(
        ConsentFactory(slug="cheese", name="Cheddar"), True, user=user
    )
    assert 0 == UserConsentUnsubscribe.objects.count()
    with freeze_time(date(2019, 7, 18)):
        UserConsentUnsubscribe.objects.create_unsubscribe_token(user, "cheese")
    assert 1 == UserConsentUnsubscribe.objects.count()
    x = UserConsentUnsubscribe.objects.first()
    assert date(2019, 7, 18) == x.created.date()
    assert date(2019, 7, 18) == x.modified.date()


@pytest.mark.django_db
def test_create_unsubscribe_url_does_not_exist():
    with pytest.raises(GdprError) as e:
        UserConsentUnsubscribe.objects.create_unsubscribe_token(
            UserFactory(username="patrick"), "does-not-exist"
        )
    assert "Cannot find a 'does-not-exist' consent for 'patrick'" in str(
        e.value
    )


@pytest.mark.django_db
@pytest.mark.parametrize(
    "date_created,expect",
    [
        (date(2019, 1, 1), False),
        (date(2019, 5, 20), True),
        (date(2018, 12, 25), False),
        (date(2019, 7, 1), True),
    ],
)
def test_is_valid_token(date_created, expect):
    user = UserFactory()
    UserConsent.objects.set_consent(
        ConsentFactory(slug="cheese"), True, user=user
    )
    assert 0 == UserConsentUnsubscribe.objects.count()
    with freeze_time(date_created):
        UserConsentUnsubscribe.objects.create_unsubscribe_token(user, "cheese")
    assert 1 == UserConsentUnsubscribe.objects.count()
    user_consent_unsubscribe = UserConsentUnsubscribe.objects.first()
    with freeze_time(date(2019, 7, 19)):
        assert user_consent_unsubscribe.is_valid_token() is expect


@pytest.mark.django_db
def test_str():
    user_consent = UserConsent.objects.set_consent(
        ConsentFactory(slug="cheese", name="Cheddar"),
        True,
        user=UserFactory(
            first_name="P", last_name="Kimber", username="patrick"
        ),
    )
    assert 0 == UserConsentUnsubscribe.objects.count()
    UserConsentUnsubscribe.objects.create_unsubscribe_token(
        user_consent.user, user_consent.consent.slug
    )
    assert 1 == UserConsentUnsubscribe.objects.count()
    user_consent_unsubscribe = UserConsentUnsubscribe.objects.first()
    assert "P Kimber - Cheddar (Token Active)" == str(user_consent_unsubscribe)


@pytest.mark.django_db
def test_init_user_consent_unsubscribe_consent_not_given():
    user_consent = UserConsent.objects.set_consent(
        ConsentFactory(slug="cheese", name="Cheddar"),
        False,
        user=UserFactory(
            first_name="P", last_name="Kimber", username="patrick"
        ),
    )
    with pytest.raises(GdprError) as e:
        UserConsentUnsubscribe.objects._create_user_consent_unsubscribe(
            user_consent, secrets.token_urlsafe()
        )
    assert (
        "'P Kimber' has not given consent for 'cheese' (id {}) so we "
        "should not be creating an unsubscribe token.".format(user_consent.pk)
    ) in str(e.value)


@pytest.mark.django_db
def test_token_is_unique():
    user_consent_1 = UserConsent.objects.set_consent(
        ConsentFactory(), True, user=UserFactory()
    )
    user_consent_2 = UserConsent.objects.set_consent(
        ConsentFactory(), True, user=UserFactory()
    )
    token = secrets.token_urlsafe()
    UserConsentUnsubscribe(user_consent=user_consent_1, token=token).save()
    with pytest.raises(IntegrityError) as e:
        UserConsentUnsubscribe(user_consent=user_consent_2, token=token).save()
    assert "duplicate key value violates unique constraint" in str(e.value)


@pytest.mark.django_db
def test_unsubscribe_comment():
    user_consent = UserConsent.objects.set_consent(
        ConsentFactory(slug="cheese", name="Cheddar"),
        True,
        user=UserFactory(
            first_name="P", last_name="Kimber", username="patrick"
        ),
    )
    assert 0 == UserConsentUnsubscribe.objects.count()
    with freeze_time("2018-01-12"):
        UserConsentUnsubscribe.objects.create_unsubscribe_token(
            user_consent.user, user_consent.consent.slug
        )
    assert 1 == UserConsentUnsubscribe.objects.count()
    x = UserConsentUnsubscribe.objects.first()
    assert "Unsubscribed on 12/01/2018" == x.unsubscribe_comment()


@pytest.mark.django_db
def test_user_consent_unique():
    """A token can only be used once."""
    user_consent = UserConsent.objects.set_consent(
        ConsentFactory(), True, user=UserFactory()
    )
    assert 0 == UserConsentUnsubscribe.objects.count()
    token = secrets.token_urlsafe()
    UserConsentUnsubscribe.objects._create_user_consent_unsubscribe(
        user_consent, token
    )
    assert 1 == UserConsentUnsubscribe.objects.count()
    with pytest.raises(IntegrityError) as e:
        UserConsentUnsubscribe.objects._create_user_consent_unsubscribe(
            user_consent, token
        )
    assert "duplicate key value violates unique constraint" in str(e.value)


@pytest.mark.django_db
def test_user_consent_unique_not():
    user_consent = UserConsent.objects.set_consent(
        ConsentFactory(), True, user=UserFactory()
    )
    assert 0 == UserConsentUnsubscribe.objects.count()
    UserConsentUnsubscribe.objects.create_unsubscribe_token(
        user_consent.user, user_consent.consent.slug
    )
    assert 1 == UserConsentUnsubscribe.objects.count()
    UserConsentUnsubscribe.objects.create_unsubscribe_token(
        user_consent.user, user_consent.consent.slug
    )
    assert 2 == UserConsentUnsubscribe.objects.count()
