# -*- encoding: utf-8 -*-
import pytest

from gdpr.models import UserConsent, UserConsentAudit
from gdpr.tests.factories import ConsentFactory
from login.tests.factories import UserFactory


@pytest.mark.django_db
def test_current():
    user = UserFactory()
    consent = ConsentFactory()
    UserConsent.objects.set_consent(consent, True, user=user, comment="A")
    UserConsent.objects.set_consent(consent, False, user=user, comment="B")
    user_consent = UserConsent.objects.set_consent(consent, True, user=user)
    assert [(True, "A"), (False, "B")] == [
        (x.consent_given, x.comment)
        for x in UserConsentAudit.objects.current(user_consent).order_by("pk")
    ]
