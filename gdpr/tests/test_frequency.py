# -*- encoding: utf-8 -*-
import pytest

from django.db.utils import IntegrityError

from gdpr.models import Consent, ConsentFrequency, Frequency
from login.tests.factories import UserFactory
from .factories import FrequencyFactory


@pytest.mark.django_db
def test_create_frequency():
    frequency = Frequency.objects.create_frequency("monthly", "Monthly")
    assert "Monthly" == frequency.name
    assert "monthly" == frequency.slug


@pytest.mark.django_db
def test_create_frequency_duplicate():
    Frequency.objects.create_frequency("weekly", "Weekly")
    with pytest.raises(IntegrityError) as e:
        Frequency.objects.create_frequency("weekly", "Weekly")
    assert "duplicate key value violates unique" in str(e.value)


@pytest.mark.django_db
def test_current():
    FrequencyFactory(slug="1")
    FrequencyFactory(slug="2").set_deleted(UserFactory())
    FrequencyFactory(slug="3")
    assert ["1", "3"] == [x.slug for x in Frequency.objects.current()]


@pytest.mark.django_db
def test_frequency():
    consent = Consent.objects.init_consent("events", "Events", "Listing", True)
    ConsentFrequency.objects.create_consent_frequency(
        consent, FrequencyFactory(name="Post")
    )
    ConsentFrequency.objects.create_consent_frequency(
        consent, FrequencyFactory(name="email")
    )
    assert ["email", "Post"] == [x.name for x in consent.frequency.all()]


@pytest.mark.django_db
def test_get_frequencies():
    consent = Consent.objects.create_consent(
        "olives", "Olive Update", "Food!", False
    )
    ConsentFrequency.objects.init_consent_frequency(
        consent, FrequencyFactory(slug="a")
    )
    ConsentFrequency.objects.init_consent_frequency(
        consent, FrequencyFactory(slug="b")
    )
    assert set(["a", "b"]) == {
        x.frequency.slug for x in consent.get_frequencies()
    }


@pytest.mark.django_db
def test_init_frequency():
    frequency = Frequency.objects.init_frequency("weekly", "Weekly")
    assert "Weekly" == frequency.name


@pytest.mark.django_db
def test_str():
    frequency = FrequencyFactory(name="Never")
    assert "Never" == str(frequency)
