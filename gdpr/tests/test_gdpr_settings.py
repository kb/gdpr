# -*- encoding: utf-8 -*-
import pytest

from gdpr.models import GdprSettings


@pytest.mark.django_db
def test_default():
    settings = GdprSettings.load()
    assert settings.can_download_csv is False


@pytest.mark.django_db
def test_default_then_update():
    settings = GdprSettings.load()
    settings.can_download_csv = True
    settings.save()
    settings.refresh_from_db()
    assert settings.can_download_csv is True


@pytest.mark.django_db
def test_str():
    settings = GdprSettings.load()
    assert "GDPR Settings: Can download CSV: False" == str(settings)
