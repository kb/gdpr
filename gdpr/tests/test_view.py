# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse
from freezegun import freeze_time
from http import HTTPStatus

from gdpr.models import (
    Consent,
    ConsentFrequency,
    ConsentFormSettings,
    ConsentMethod,
    Frequency,
    Method,
    UserConsent,
)
from gdpr.tests.factories import (
    ConsentFactory,
    ConsentFormSettingsFactory,
    FrequencyFactory,
    GdprSettingsFactory,
    MethodFactory,
)
from login.tests.factories import TEST_PASSWORD, UserFactory


@pytest.mark.django_db
def test_consent_list(client):
    MethodFactory(name="Information")
    MethodFactory(name="Marketing").set_deleted(UserFactory())
    MethodFactory(name="Products")
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(reverse("gdpr.method.list"))
    assert 200 == response.status_code
    assert "object_list" in response.context
    assert ["Information", "Products"] == [
        x.name for x in response.context["object_list"]
    ]


@pytest.mark.django_db
def test_consent_create(client):
    GdprSettingsFactory(show_frequency=True, show_method=True)
    frequency = FrequencyFactory()
    method = MethodFactory()
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    data = {
        "slug": "product",
        "name": "Product News",
        "description": "Information on our latest products",
        "consent_methods": [method.pk],
        "consent_frequencies": [frequency.pk],
        "show_checkbox": False,
        "message": "Do you want to know about offers on cake?",
        "no_consent_message": "Are you sure?",
        "consent_required": True,
    }
    response = client.post(reverse("gdpr.consent.create"), data)
    assert 302 == response.status_code
    assert 1 == Consent.objects.count()
    consent = Consent.objects.first()
    assert reverse("gdpr.consent.detail", args=[consent.pk]) == response.url
    assert consent.show_checkbox is False
    assert consent.consent_required is True
    assert "Product News" == consent.name
    assert "Information on our latest products" == consent.description
    assert "Do you want to know about offers on cake?" == consent.message
    assert "Are you sure?" == consent.no_consent_message
    assert [method.pk] == [x.method.pk for x in consent.get_methods()]
    assert [frequency.pk] == [x.frequency.pk for x in consent.get_frequencies()]


@pytest.mark.django_db
def test_consent_update(client):
    GdprSettingsFactory(show_frequency=True, show_method=True)
    u = UserFactory(is_staff=True)
    method_1 = MethodFactory()
    method_2 = MethodFactory()
    frequency_1 = FrequencyFactory()
    frequency_2 = FrequencyFactory()
    consent = ConsentFactory(show_checkbox=True)
    consent_method = ConsentMethod.objects.init_consent_method(
        consent, method_2
    )
    assert consent_method.is_deleted is False
    consent_frequency = ConsentFrequency.objects.init_consent_frequency(
        consent, frequency_2
    )
    assert consent_frequency.is_deleted is False
    assert consent.show_checkbox is True
    assert consent.consent_required is False
    assert client.login(username=u.username, password=TEST_PASSWORD) is True
    data = {
        "slug": "product",
        "name": "Product News",
        "description": "Information on our latest products",
        "consent_methods": [method_1.pk],
        "consent_frequencies": [frequency_1.pk],
        "show_checkbox": False,
        "message": "Do you want to know about offers on cake?",
        "no_consent_message": "Are you sure?",
        "consent_required": True,
    }
    response = client.post(
        reverse("gdpr.consent.update", args=[consent.pk]), data
    )
    assert 302 == response.status_code
    assert reverse("gdpr.consent.detail", args=[consent.pk]) == response.url
    consent.refresh_from_db()
    assert consent.show_checkbox is False
    assert consent.consent_required is True
    assert "Product News" == consent.name
    assert "Information on our latest products" == consent.description
    assert "Do you want to know about offers on cake?" == consent.message
    assert "Are you sure?" == consent.no_consent_message
    assert [method_1.pk] == [x.method.pk for x in consent.get_methods()]
    assert [frequency_1.pk] == [
        x.frequency.pk for x in consent.get_frequencies()
    ]
    consent_method.refresh_from_db()
    assert consent_method.is_deleted is True
    consent_frequency.refresh_from_db()
    assert consent_frequency.is_deleted is True


@pytest.mark.django_db
def test_consent_update_no_description(client):
    u = UserFactory(is_staff=True)
    consent = ConsentFactory()
    assert client.login(username=u.username, password=TEST_PASSWORD) is True
    data = {
        "slug": "product",
        "name": "Product News",
        "message": "Offers on cake?",
    }
    response = client.post(
        reverse("gdpr.consent.update", args=[consent.pk]), data
    )
    assert 302 == response.status_code
    assert reverse("gdpr.consent.detail", args=[consent.pk]) == response.url
    consent.refresh_from_db()
    assert "" == consent.description


@pytest.mark.django_db
def test_consent_update_consent_required(client):
    u = UserFactory(is_staff=True)
    consent = ConsentFactory()
    assert client.login(username=u.username, password=TEST_PASSWORD) is True
    data = {
        "slug": "product",
        "name": "Product News",
        "message": "Offers on cake?",
        "consent_required": True,
        "no_consent_message": "",
    }
    response = client.post(
        reverse("gdpr.consent.update", args=[consent.pk]), data
    )
    assert HTTPStatus.OK == response.status_code
    assert {
        "__all__": [
            (
                "You have ticked the box to say consent is required. "
                "Please enter a 'No consent message'."
            )
        ]
    } == response.context["form"].errors


@pytest.mark.django_db
def test_consent_update_consent_required_not(client):
    u = UserFactory(is_staff=True)
    consent = ConsentFactory()
    assert client.login(username=u.username, password=TEST_PASSWORD) is True
    data = {
        "slug": "product",
        "name": "Product News",
        "message": "Offers on cake?",
        "consent_required": False,
        "no_consent_message": "",
    }
    response = client.post(
        reverse("gdpr.consent.update", args=[consent.pk]), data
    )
    assert 302 == response.status_code
    assert reverse("gdpr.consent.detail", args=[consent.pk]) == response.url
    consent.refresh_from_db()
    assert "" == consent.no_consent_message


@pytest.mark.django_db
def test_consent_user_list(client):
    consent = ConsentFactory()
    UserConsent.objects.set_consent(
        consent, True, user=UserFactory(username="A")
    )
    UserConsent.objects.set_consent(
        consent, True, user=UserFactory(username="B")
    )
    # login
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(reverse("gdpr.consent.user.list", args=[consent.pk]))
    # check
    assert 200 == response.status_code
    assert "object_list" in response.context
    assert ["B", "A"] == [
        x.user.username for x in response.context["object_list"]
    ]


@pytest.mark.django_db
def test_form_settings_create(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("gdpr.form.settings.create")
    response = client.post(
        url, data={"slug": "contact", "form_notice": "Apple"}
    )
    # check
    assert 302 == response.status_code
    assert reverse("gdpr.form.settings.list") == response.url
    assert 1 == ConsentFormSettings.objects.count()
    form_settings = ConsentFormSettings.objects.first()
    assert form_settings.form_notice == "Apple"
    assert form_settings.slug == "contact"


@pytest.mark.django_db
def test_form_settings_list(client):
    user = UserFactory(is_staff=True)
    ConsentFormSettingsFactory(slug="A")
    ConsentFormSettingsFactory(slug="B")
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(reverse("gdpr.form.settings.list"))
    # check
    assert 200 == response.status_code
    assert "object_list" in response.context
    assert ["A", "B"] == [x.slug for x in response.context["object_list"]]


@pytest.mark.django_db
def test_form_settings_update(client):
    form_settings = ConsentFormSettingsFactory(
        slug="enquiry", form_notice="Note"
    )
    assert "Note" == form_settings.form_notice
    assert "enquiry" == form_settings.slug
    # login
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("gdpr.form.settings.update", args=[form_settings.pk])
    response = client.post(
        url, data={"slug": "enquiry", "form_notice": "Notice"}
    )
    # check
    assert 302 == response.status_code
    assert reverse("gdpr.form.settings.list") == response.url
    form_settings.refresh_from_db()
    assert form_settings.form_notice == "Notice"
    assert form_settings.slug == "enquiry"


@pytest.mark.django_db
def test_frequency_create(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("gdpr.frequency.create")
    response = client.post(url, data={"slug": "week", "name": "Weekly"})
    # check
    assert 302 == response.status_code
    assert reverse("gdpr.frequency.list") == response.url
    assert 1 == Frequency.objects.count()
    frequency = Frequency.objects.first()
    assert frequency.is_deleted is False
    assert frequency.name == "Weekly"
    assert frequency.slug == "week"


@pytest.mark.django_db
def test_frequency_list(client):
    user = UserFactory(is_staff=True)
    FrequencyFactory(name="A")
    FrequencyFactory().set_deleted(user)
    FrequencyFactory(name="C")
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(reverse("gdpr.frequency.list"))
    # check
    assert 200 == response.status_code
    assert "object_list" in response.context
    assert ["A", "C"] == [x.name for x in response.context["object_list"]]


@pytest.mark.django_db
def test_frequency_delete(client):
    frequency = FrequencyFactory()
    assert frequency.is_deleted is False
    # login
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("gdpr.frequency.delete", args=[frequency.pk])
    response = client.post(url)
    # check
    assert 302 == response.status_code
    assert reverse("gdpr.frequency.list") == response.url
    frequency.refresh_from_db()
    assert frequency.is_deleted is True


@pytest.mark.django_db
def test_frequency_update(client):
    frequency = FrequencyFactory(slug="week", name="Weak")
    assert frequency.is_deleted is False
    assert "Weak" == frequency.name
    assert "week" == frequency.slug
    # login
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("gdpr.frequency.update", args=[frequency.pk])
    response = client.post(url, data={"slug": "week", "name": "Weekly"})
    # check
    assert 302 == response.status_code
    assert reverse("gdpr.frequency.list") == response.url
    frequency.refresh_from_db()
    assert frequency.is_deleted is False
    assert frequency.name == "Weekly"
    assert frequency.slug == "week"


@pytest.mark.django_db
def test_method_create(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("gdpr.method.create")
    response = client.post(url, data={"slug": "post", "name": "Postal"})
    # check
    assert 302 == response.status_code
    assert reverse("gdpr.method.list") == response.url
    assert 1 == Method.objects.count()
    method = Method.objects.first()
    assert method.is_deleted is False
    assert method.name == "Postal"
    assert method.slug == "post"


@pytest.mark.django_db
def test_method_list(client):
    user = UserFactory(is_staff=True)
    MethodFactory(name="A")
    MethodFactory().set_deleted(user)
    MethodFactory(name="C")
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(reverse("gdpr.method.list"))
    # check
    assert 200 == response.status_code
    assert "object_list" in response.context
    assert ["A", "C"] == [x.name for x in response.context["object_list"]]


@pytest.mark.django_db
def test_method_delete(client):
    method = MethodFactory()
    assert method.is_deleted is False
    # login
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("gdpr.method.delete", args=[method.pk])
    response = client.post(url)
    # check
    assert 302 == response.status_code
    assert reverse("gdpr.method.list") == response.url
    method.refresh_from_db()
    assert method.is_deleted is True


@pytest.mark.django_db
def test_method_update(client):
    method = MethodFactory(slug="post", name="Post")
    assert method.is_deleted is False
    assert "Post" == method.name
    assert "post" == method.slug
    # login
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("gdpr.method.update", args=[method.pk])
    response = client.post(url, data={"slug": "post2", "name": "Postal"})
    # check
    assert 302 == response.status_code
    assert reverse("gdpr.method.list") == response.url
    method.refresh_from_db()
    assert method.is_deleted is False
    assert method.name == "Postal"
    assert method.slug == "post2"


@pytest.mark.django_db
def test_user_consent_create(client):
    ConsentFactory()
    consent_2 = ConsentFactory()
    # login
    staff = UserFactory(is_staff=True)
    assert client.login(username=staff.username, password=TEST_PASSWORD) is True
    assert 0 == UserConsent.objects.count()
    user = UserFactory()
    response = client.post(
        reverse("gdpr.user.consent.create", args=[user.pk]),
        {
            "consent": consent_2.pk,
            "consent_given": True,
            "comment": "email received",
        },
    )
    # check
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert reverse("user.redirect.contact", args=[user.pk]) == response.url
    assert 1 == UserConsent.objects.count()
    user_consent = UserConsent.objects.first()
    assert user == user_consent.user
    assert consent_2.pk == user_consent.consent.pk
    assert user_consent.consent_given is True
    assert staff == user_consent.user_updated
    assert "email received" == user_consent.comment


@pytest.mark.django_db
def test_user_consent_create_missing_comment(client):
    ConsentFactory()
    consent_2 = ConsentFactory()
    # login
    staff = UserFactory(is_staff=True)
    assert client.login(username=staff.username, password=TEST_PASSWORD) is True
    assert 0 == UserConsent.objects.count()
    user = UserFactory()
    response = client.post(
        reverse("gdpr.user.consent.create", args=[user.pk]),
        {"consent": consent_2.pk, "consent_given": True},
    )
    # check
    assert HTTPStatus.OK == response.status_code
    assert {"comment": ["This field is required."]} == response.context[
        "form"
    ].errors


@pytest.mark.django_db
def test_user_consent_create_exclude_existing(client):
    """If a user has an existing consent, then don't include it in the list."""
    consent_1 = ConsentFactory()
    consent_2 = ConsentFactory()
    consent_3 = ConsentFactory()
    user = UserFactory()
    UserConsent.objects.set_consent(consent_2, False, user=user)
    # login
    staff = UserFactory(is_staff=True)
    assert client.login(username=staff.username, password=TEST_PASSWORD) is True
    response = client.get(reverse("gdpr.user.consent.create", args=[user.pk]))
    # check
    assert HTTPStatus.OK == response.status_code
    form = response.context["form"]
    assert "consent" in form.fields
    f = form.fields["consent"]
    assert [consent_1.pk, consent_3.pk] == [x.pk for x in f.queryset]


@pytest.mark.django_db
def test_user_consent_download(client):
    consent = ConsentFactory()
    with freeze_time("2017-05-21"):
        user_1 = UserFactory(
            first_name="A", last_name="Kimber", email="a@test.com"
        )
        UserConsent.objects.set_consent(consent, True, user=user_1)
        user_2 = UserFactory(
            first_name="B", last_name="Orange", email="b@b.com"
        )
        UserConsent.objects.set_consent(consent, False, user=user_2)
    # login
    staff = UserFactory(is_staff=True)
    assert client.login(username=staff.username, password=TEST_PASSWORD) is True
    response = client.get(
        reverse("gdpr.consent.user.download", args=[consent.pk])
    )
    assert HTTPStatus.OK == response.status_code
    assert (
        b"name,email,date,consent\r\n"
        b"A Kimber,a@test.com,21/05/2017,Y\r\n"
        b"B Orange,b@b.com,21/05/2017,\r\n"
    ) == response.content


@pytest.mark.django_db
def test_user_consent_update(client):
    ConsentFactory()
    consent_2 = ConsentFactory()
    user = UserFactory()
    staff = UserFactory(is_staff=True)
    user_consent = UserConsent.objects.set_consent(
        consent_2, False, user=user, user_updated=UserFactory()
    )
    # login
    assert client.login(username=staff.username, password=TEST_PASSWORD) is True
    assert 1 == UserConsent.objects.count()
    response = client.post(
        reverse("gdpr.user.consent.update", args=[user_consent.pk]),
        {"consent_given": True, "comment": "Post"},
    )
    # check
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert reverse("user.redirect.contact", args=[user.pk]) == response.url
    assert 1 == UserConsent.objects.count()
    user_consent = UserConsent.objects.first()
    assert user == user_consent.user
    assert consent_2.pk == user_consent.consent.pk
    assert user_consent.consent_given is True
    assert staff == user_consent.user_updated
    assert "Post" == user_consent.comment


@pytest.mark.django_db
def test_user_consent_update_blank_comment(client):
    """When editing a user consent, the previous comment must be cleared."""
    consent = ConsentFactory()
    user = UserFactory()
    staff = UserFactory(is_staff=True)
    user_consent = UserConsent.objects.set_consent(
        consent, False, user=user, comment="email"
    )
    # login
    assert client.login(username=staff.username, password=TEST_PASSWORD) is True
    assert 1 == UserConsent.objects.count()
    response = client.get(
        reverse("gdpr.user.consent.update", args=[user_consent.pk])
    )
    # check
    assert HTTPStatus.OK == response.status_code
    form = response.context["form"]
    assert "comment" in form.initial
    assert "" == form.initial["comment"]


@pytest.mark.django_db
def test_user_consent_update_missing_comment(client):
    ConsentFactory()
    consent_2 = ConsentFactory()
    user = UserFactory()
    staff = UserFactory(is_staff=True)
    user_consent = UserConsent.objects.set_consent(
        consent_2, False, user=user, user_updated=UserFactory()
    )
    # login
    assert client.login(username=staff.username, password=TEST_PASSWORD) is True
    assert 1 == UserConsent.objects.count()
    response = client.post(
        reverse("gdpr.user.consent.update", args=[user_consent.pk]),
        {"consent_given": True},
    )
    assert HTTPStatus.OK == response.status_code
    assert {"comment": ["This field is required."]} == response.context[
        "form"
    ].errors
