# -*- encoding: utf-8 -*-
"""
Test the ``UserConsent`` model and manager.

For other tests, see:
example_gdpr/tests/test_user_consent.py

"""
import pytest
import pytz

from datetime import datetime
from dateutil.relativedelta import relativedelta
from django.utils import timezone

from gdpr.models import (
    Frequency,
    GdprError,
    Method,
    UserConsent,
    UserConsentAudit,
)
from gdpr.tests.factories import ConsentFactory, FrequencyFactory, MethodFactory
from login.tests.factories import UserFactory


@pytest.mark.django_db
@pytest.mark.parametrize(
    "consent_given,comment,minutes_to_add,expected",
    [
        (True, "Apple", None, True),
        (False, "Apple", None, False),
        (True, "Apple", 1, False),
        (True, "Orange", None, False),
        (True, "Orange", None, False),
    ],
)
def test_consent_exists_for_user(
    consent_given, comment, minutes_to_add, expected
):
    """Test variations of consent given, date updated and comments."""
    consent = ConsentFactory()
    date_updated = timezone.now()
    user = UserFactory()
    UserConsent.objects.set_consent(
        consent, True, date_updated=date_updated, comment="Apple", user=user
    )
    check_date = date_updated
    if minutes_to_add:
        check_date = date_updated + relativedelta(minutes=+1)
    assert (
        UserConsent.objects.consent_exists_for_user(
            consent, user, consent_given, check_date, comment
        )
        is expected
    )


@pytest.mark.django_db
@pytest.mark.parametrize(
    "consent_given,comment,minutes_to_add,expected",
    [
        (True, "Apple", None, True),
        (False, "Apple", None, False),
        (True, "Apple", 1, False),
        (True, "Orange", None, False),
        (True, "Orange", None, False),
    ],
)
def test_consent_exists_for_user_in_audit(
    consent_given, comment, minutes_to_add, expected
):
    """Test variations of consent given, date updated and comments."""
    consent = ConsentFactory()
    date_updated = timezone.now()
    user = UserFactory()
    UserConsentAudit(
        user=user,
        consent=consent,
        consent_given=True,
        date_updated=date_updated,
        comment="Apple",
    ).save()
    check_date = date_updated
    if minutes_to_add:
        check_date = date_updated + relativedelta(minutes=+1)
    assert (
        UserConsent.objects.consent_exists_for_user(
            consent, user, consent_given, check_date, comment
        )
        is expected
    )


@pytest.mark.django_db
def test_consent_given_for_all():
    user = UserFactory()
    UserConsent.objects.set_consent(ConsentFactory(), True, user=user)
    assert UserConsent.objects.consent_given_for_all(user) is True


@pytest.mark.django_db
def test_consent_given_for_all_none_given():
    assert UserConsent.objects.consent_given_for_all(UserFactory()) is False


@pytest.mark.django_db
def test_consent_given_for_all_one_but_not_the_other():
    user = UserFactory()
    UserConsent.objects.set_consent(ConsentFactory(), True, user=user)
    UserConsent.objects.set_consent(ConsentFactory(), False, user=user)
    assert UserConsent.objects.consent_given_for_all(user) is False


@pytest.mark.django_db
def test_current():
    consent = ConsentFactory()
    UserConsent.objects.set_consent(
        consent, True, user=UserFactory(username="a")
    )
    UserConsent.objects.set_consent(
        consent, False, user=UserFactory(username="b")
    )
    UserConsent.objects.set_consent(
        consent, False, user=UserFactory(username="c")
    )
    assert ["a", "b", "c"] == [
        x.user.username
        for x in UserConsent.objects.current(consent).order_by("pk")
    ]


@pytest.mark.django_db
def test_for_user():
    user = UserFactory()
    UserConsent.objects.set_consent(
        ConsentFactory(),
        True,
        user=user,
        user_updated=UserFactory(username="a"),
    )
    UserConsent.objects.set_consent(
        ConsentFactory(),
        True,
        user=UserFactory(),
        user_updated=UserFactory(username="b"),
    )
    UserConsent.objects.set_consent(
        ConsentFactory(),
        False,
        user=user,
        user_updated=UserFactory(username="c"),
    )
    assert ["a", "c"] == [
        x.user_updated.username
        for x in UserConsent.objects.for_user(user).order_by(
            "user_updated__username"
        )
    ]


@pytest.mark.django_db
def test_get_email_for_user():
    """The email address for a user might be in the user or contact record.

    .. note:: This test should get the email address for the user.

    .. note:: The test for getting the email address from the contact is in
              ``example_gdpr/tests/test_user_consent.py``

    """
    consent = ConsentFactory()
    user = UserFactory(email="patrick@kbsoftware.co.uk")
    UserConsent.objects.set_consent(consent, True, user=user)
    assert "patrick@kbsoftware.co.uk" == UserConsent.objects.get_email_for_user(
        user
    )


@pytest.mark.django_db
def test_init_user_consent():
    consent = ConsentFactory()
    user = UserFactory()
    user_updated = UserFactory()
    user_consent = UserConsent.objects.set_consent(
        consent, True, user=user, user_updated=user_updated
    )
    user_consent.refresh_from_db()
    assert consent == user_consent.consent
    assert user == user_consent.user
    assert user_consent.consent_given is True
    assert user_updated == user_consent.user_updated
    assert timezone.now().date() == user_consent.date_updated.date()


@pytest.mark.django_db
def test_init_user_consent_method_freqency():
    consent = ConsentFactory()
    user = UserFactory()
    methods = [MethodFactory(name="A"), MethodFactory(name="B")]
    frequencies = [FrequencyFactory(name="C"), FrequencyFactory(name="D")]
    user_consent = UserConsent.objects.set_consent(
        consent, True, user=user, methods=methods, frequencies=frequencies
    )
    user_consent.refresh_from_db()
    assert consent == user_consent.consent
    assert user == user_consent.user
    assert user_consent.consent_given is True
    assert user == user_consent.user_updated
    assert timezone.now().date() == user_consent.date_updated.date()
    assert ["C", "D"] == [x.name for x in user_consent.frequencies.all()]
    assert ["A", "B"] == [x.name for x in user_consent.methods.all()]


@pytest.mark.django_db
def test_init_user_consent_remove_method_freqency():
    consent = ConsentFactory()
    user = UserFactory()
    frequency_d = FrequencyFactory(name="D")
    frequency_e = FrequencyFactory(name="E")
    frequency_f = FrequencyFactory(name="F")
    method_a = MethodFactory(name="A")
    method_b = MethodFactory(name="B")
    method_c = MethodFactory(name="C")
    user_consent = UserConsent.objects.set_consent(
        consent,
        True,
        user=user,
        frequencies=[frequency_d, frequency_e],
        methods=[method_a, method_b],
    )
    user_consent.refresh_from_db()
    assert ["D", "E"] == [x.name for x in user_consent.frequencies.all()]
    assert ["A", "B"] == [x.name for x in user_consent.methods.all()]
    user_consent = UserConsent.objects.set_consent(
        consent,
        True,
        user=user,
        frequencies=[frequency_e, frequency_f],
        methods=[method_b, method_c],
    )
    user_consent.refresh_from_db()
    assert ["E", "F"] == [x.name for x in user_consent.frequencies.all()]
    assert ["B", "C"] == [x.name for x in user_consent.methods.all()]


@pytest.mark.django_db
def test_is_consent():
    consent = ConsentFactory()
    user = UserFactory()
    UserConsent.objects.set_consent(consent, True, user=user)
    assert UserConsent.objects.consent_given(user, consent) is True


@pytest.mark.django_db
def test_is_consent_no_data():
    consent = ConsentFactory()
    user = UserFactory()
    assert UserConsent.objects.consent_given(user, consent) is False


@pytest.mark.django_db
def test_is_consent_no_data_for_consent():
    consent = ConsentFactory()
    user = UserFactory()
    UserConsent.objects.set_consent(consent, True, user=user)
    assert UserConsent.objects.consent_given(user, ConsentFactory()) is False


@pytest.mark.django_db
def test_set_consent():
    consent = ConsentFactory()
    user = UserFactory()
    user_updated = UserFactory()
    UserConsent.objects.set_consent(
        consent, True, user=user, user_updated=user_updated, comment="Post"
    )
    assert UserConsent.objects.consent_given(user, consent) is True
    assert 1 == UserConsent.objects.count()
    user_consent = UserConsent.objects.first()
    assert user_consent.consent_given is True
    assert timezone.now().date() == user_consent.date_updated.date()
    assert user_updated == user_consent.user_updated
    assert user == user_consent.user
    assert "Post" == user_consent.comment


@pytest.mark.django_db
def test_set_consent_audit():
    consent = ConsentFactory()
    user = UserFactory()
    user_updated_1 = UserFactory()
    user_updated_2 = UserFactory()
    frequency_1 = FrequencyFactory()
    frequency_2 = FrequencyFactory()
    method_1 = MethodFactory()
    method_2 = MethodFactory()
    UserConsent.objects.set_consent(
        consent,
        True,
        user=user,
        user_updated=user_updated_1,
        frequencies=[frequency_1, frequency_2],
        methods=[method_1, method_2],
    )
    assert UserConsent.objects.consent_given(user, consent) is True
    assert 1 == UserConsent.objects.count()
    user_consent = UserConsent.objects.first()
    assert user_consent.consent_given is True
    assert timezone.now().date() == user_consent.date_updated.date()
    assert user_updated_1 == user_consent.user_updated
    assert user == user_consent.user
    assert [frequency_1.pk, frequency_2.pk] == [
        x.pk for x in user_consent.frequencies.all().order_by("pk")
    ]
    assert [method_1.pk, method_2.pk] == [
        x.pk for x in user_consent.methods.all().order_by("pk")
    ]
    # action
    UserConsent.objects.set_consent(
        consent,
        False,
        user=user,
        user_updated=user_updated_2,
        frequencies=[frequency_2],
        methods=[method_2],
    )
    # check
    assert 2 == Frequency.objects.count()
    assert 2 == Method.objects.count()
    assert UserConsent.objects.consent_given(user, consent) is False
    assert 1 == UserConsent.objects.count()
    user_consent = UserConsent.objects.first()
    assert user_consent.consent_given is False
    assert timezone.now().date() == user_consent.date_updated.date()
    assert user_updated_2 == user_consent.user_updated
    assert user == user_consent.user
    assert [frequency_2.pk] == [
        x.pk for x in user_consent.frequencies.all().order_by("pk")
    ]
    assert [method_2.pk] == [
        x.pk for x in user_consent.methods.all().order_by("pk")
    ]
    # check audit
    assert 1 == UserConsentAudit.objects.count()
    audit = UserConsentAudit.objects.first()
    assert audit.consent_given is True
    assert timezone.now().date() == audit.date_updated.date()
    assert user_updated_1 == audit.user_updated
    assert user == audit.user
    assert [frequency_1.pk, frequency_2.pk] == [
        x.pk for x in audit.frequencies.all().order_by("pk")
    ]
    assert [method_1.pk, method_2.pk] == [
        x.pk for x in audit.methods.all().order_by("pk")
    ]
    # action
    UserConsent.objects.set_consent(consent, True, user=user)
    # check audit
    assert 2 == UserConsentAudit.objects.count()


@pytest.mark.django_db
def test_set_consent_date_updated():
    assert 0 == UserConsent.objects.count()
    date_updated = datetime(2015, 11, 29, 20, 13, 5, tzinfo=pytz.utc)
    UserConsent.objects.set_consent(
        ConsentFactory(), True, user=UserFactory(), date_updated=date_updated
    )
    assert 1 == UserConsent.objects.count()
    user_consent = UserConsent.objects.first()
    assert date_updated == user_consent.date_updated


@pytest.mark.django_db
def test_set_consent_old_record():
    """Handle old user consent records when importing from MailChimp."""
    assert 0 == UserConsent.objects.count()
    assert 0 == UserConsentAudit.objects.count()
    consent = ConsentFactory()
    user = UserFactory()
    UserConsent.objects.set_consent(consent, True, user=user, comment="today")
    # add a consent with an old date
    date_updated = datetime(2015, 11, 29, 20, 13, 5, tzinfo=pytz.utc)
    UserConsent.objects.set_consent(
        consent, True, user=user, date_updated=date_updated, comment="2015"
    )
    assert 1 == UserConsent.objects.count()
    assert 1 == UserConsentAudit.objects.count()
    user_consent = UserConsent.objects.first()
    assert "today" == user_consent.comment
    assert timezone.now().date() == user_consent.date_updated.date()
    # the consent with the old date should be added to the audit
    user_consent_audit = UserConsentAudit.objects.first()
    assert "2015" == user_consent_audit.comment
    assert date_updated == user_consent_audit.date_updated


@pytest.mark.django_db
def test_str():
    consent = ConsentFactory(slug="cheese", name="Cheddar")
    user = UserFactory(first_name="P", last_name="Kimber", username="patrick")
    user_consent = UserConsent.objects.set_consent(consent, False, user=user)
    assert "P Kimber - Cheddar - No" == str(user_consent)


@pytest.mark.django_db
def test_update_user_for_content_object():
    consent = ConsentFactory()
    with pytest.raises(GdprError) as e:
        UserConsent.objects.set_consent(consent, True)
    assert "To record consent, we need a user or 'content_object'" in str(
        e.value
    )


@pytest.mark.django_db
def test_user_consent_given():
    """Which users have given consent?"""
    consent = ConsentFactory()
    user_a = UserFactory(username="a")
    user_b = UserFactory(username="b")
    user_c = UserFactory(username="c")
    UserFactory(username="d")
    UserConsent.objects.set_consent(consent, True, user=user_a)
    UserConsent.objects.set_consent(consent, False, user=user_b)
    UserConsent.objects.set_consent(consent, True, user=user_c)
    assert set([user_a.pk, user_c.pk]) == set(
        UserConsent.objects.user_consent_given(consent)
    )
